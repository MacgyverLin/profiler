/*
var http = require('http');
var fs = require('fs');
var dt = require('./myfirstmodule');

http.createServer(function (req, res) {
    fs.readFile('demofile1.html', function (err, data) {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(data);
        res.write(dt.myDateTime())
        res.end();
    });
}).listen(8080);
*/

const http = require('http');
const fs = require('fs');
const FrameProRecording = require('./FrameProRecording');
const config = require('./Config');

const filename = "mac.framepro_recording";

fs.readFile(filename, function (err, data) {
    // load data
    var frameProRecording = new FrameProRecording();
    frameProRecording.build(data);

    // get TimeSpan Data Sorted by thread Id
    var timeSpanMap = frameProRecording.getTimeSpan();
    timeSpanMap.forEach(
        function (value, key, map) {
            value.forEach(
                function (v, index, array) {
                    console.log(v.threadid + " " + v.threadName + " " + v.name + " " + v.totalTime + " " + v.selfTime);
                }
            );
        }
    );

    // get TimeSpan Data Sorted by thread Id then sorted by Total Time
    timeSpanMap = frameProRecording.getTimeSpanSortedByTotalTime();
    timeSpanMap.forEach(
        function (value, key, map) {
            value.forEach(
                function (v, index, array) {
                    console.log(v.threadid + " " + v.threadName + " " + v.name + " " + v.totalTime + " " + v.selfTime);
                }
            );
        }
    );

    // get TimeSpan Data Sorted by thread Id then sorted by Self Time
    timeSpanMap = frameProRecording.getTimeSpanSortedBySelfTime();
    timeSpanMap.forEach(
        function (value, key, map) {
            value.forEach(
                function (v, index, array) {
                    console.log(v.threadid + " " + v.threadName + " " + v.name + " " + v.totalTime + " " + v.selfTime);
                }
            );
        }
    );
});