class FileBuffer:
    def __init__(self, buffer):
        self.buffer = buffer
        self.offset = 0

    def readUInt8(self):
        val = self.peekUInt8()
        self.offset += 1
        return val

    def readUInt16(self):
        val = self.peekUInt16()
        self.offset += 2
        return val

    def readUInt32(self):
        val = self.peekUInt32()
        self.offset += 4
        return val

    def readUInt64(self):
        val = self.peekUInt64()
        self.offset += 8
        return val

    def readInt8(self):
        val = self.peekInt8()
        self.offset += 1
        return val

    def readInt16(self):
        val = self.peekInt16()
        self.offset += 2
        return val

    def readInt32(self):
        val = self.peekInt32()
        self.offset += 4
        return val

    def readInt64(self):
        val = self.peekInt64()
        self.offset += 8
        return val

    def readFloat(self):
        val = self.peekFloat()
        self.offset += 4
        return val

    def readDouble(self):
        val = self.peekDouble()
        self.offset += 8
        return val

    def readEnum(self):
        return self.readUInt32()

    def readString(self, size):
        val = self.peekString(size)
        self.offset += size
        return val

    def readBuffer(self, size):
        val = self.peekBuffer(size)
        self.offset += size
        return val

    def peekUInt8(self):
        val = self.buffer.readUInt8(self.offset)
        return val

    def peekUInt16(self):
        val = self.buffer.readUInt16LE(self.offset)
        return val

    def peekUInt32(self):
        val = self.buffer.readUInt32LE(self.offset)
        return val

    def peekUInt64(self):
        val = [self.buffer.readUInt32LE(self.offset), self.buffer.readUInt32LE(self.offset + 4)]
        return val

    def peekInt8(self):
        val = self.buffer.readInt8(self.offset)
        return val

    def peekInt16(self):
        val = self.buffer.readInt16LE(self.offset)
        return val

    def peekInt32(self):
        val = self.buffer.readInt32LE(self.offset)
        return val
    
    def peekInt64(self):
        val = [self.buffer.readInt32LE(self.offset), self.buffer.readInt32LE(self.offset + 4)]
        return val

    def peekFloat(self):
        val = self.buffer.readFloatLE(self.offset)
        return val

    def peekDouble(self):
        val = self.buffer.readDoubleLE(self.offset)
        return val
    
    def peekEnum(self):
        return self.peekUInt32()

    def peekString(self, size):
        val = self.buffer.toString('ascii', self.offset, self.offset + size)
        return val

    def peekBuffer(self, size):
        return self.peekString(size)

    def length(self):
        return self.buffer.length

    def eof(self):
        return self.offset >= self.buffer.length