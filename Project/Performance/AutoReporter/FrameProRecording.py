from enum import Enum
import FileBuffer

class PacketType(Enum):
    Connect = 0xaabb
    FrameStart = 0xaabc
    TimeSpan = 0xaabd
    TimeSpanW = 0xaabe
    NamedTimeSpan = 0xaabf
    StringLiteralNamedTimeSpan = 0xaac0
    ThreadName = 0xaac1
    ThreadOrder = 0xaac2
    StringPacket = 0xaac3
    WStringPacket = 0xaac4
    NameAndSourceInfoPacket = 0xaac5
    NameAndSourceInfoPacketW = 0xaac6
    SourceInfoPacket = 0xaac7
    MainThreadPacket = 0xaac8
    RequestStringLiteralPacket = 0xaac9
    SetConditionalScopeMinTimePacket = 0xaaca
    ConnectResponsePacket = 0xaacb
    SessionInfoPacket = 0xaacc
    RequestRecordedDataPacket = 0xaacd
    SessionDetailsPacket = 0xaace
    ContextSwitchPacket = 0xaacf
    ContextSwitchRecordingStartedPacket = 0xaad0
    ProcessNamePacket = 0xaad1
    CustomStatPacket_Depreciated = 0xaad2
    StringLiteralTimerNamePacket = 0xaad3
    HiResTimerScopePacket = 0xaad4
    LogPacket = 0xaad5
    EventPacket = 0xaad6
    StartWaitEventPacket = 0xaad7
    StopWaitEventPacket = 0xaad8
    TriggerWaitEventPacket = 0xaad9
    TimeSpanCustomStatPacket_Depreciated = 0xaada
    TimeSpanWithCallstack = 0xaadb
    TimeSpanWWithCallstack = 0xaadc
    NamedTimeSpanWithCallstack = 0xaadd
    StringLiteralNamedTimeSpanWithCallstack = 0xaade
    ModulePacket = 0xaadf
    SetCallstackRecordingEnabledPacket = 0xaae0
    CustomStatPacketW = 0xaae1
    TimeSpanCustomStatPacketW = 0xaae2
    CustomStatPacket = 0xaae3
    TimeSpanCustomStatPacket = 0xaae4
    ScopeColourPacket = 0xaae5
    CustomStatColourPacket = 0xaae6
    CustomStatGraphPacket = 0xaae7
    CustomStatUnitPacket = 0xaae8
    IDSProfileCustomEventPacket = 0xaae9
    IDSProfileSummaryEventPacket = 0xaaea
    IDSProfileFrameDurationEventPacket = 0xaaeb
    IDSProfileCPUInfoEventPacket = 0xaaec
    IDSProfileGPUInfoEventPacket = 0xaaed
    IDSProfileSFXInfoEventPacket = 0xaaee
    IDSProfileDeviceTemperatureEventPacket = 0xaaef
    IDSProfileGPUShaderUpdateEventPacket = 0xaaf0
    IDSProfileGPUTextureUpdateEventPacket = 0xaaf1
    IDSProfileGPUVertexBufferUpdateEventPacket = 0xaaf2
    IDSProfileGPUIndexBufferUpdateEventPacket = 0xaaf3
    IDSProfileGPUDrawCallUpdateEventPacket = 0xaaf4
    IDSProfileSFXBufferUpdateEventPacket = 0xaaf5

class Platform(Enum):
    Windows = 0
    Windows_UWP = 1
    XBoxOne = 2
    Unused = 3
    Linux = 4
    PS4 = 5
    Android = 6
    Mac = 7
    iOS = 8
    Switch = 9

FRAMEPRO_MAX_INLINE_STRING_LENGTH = 256

def isPow2(value):
    return (value & (value - 1)) == 0

def alignUpPow2(value, alignment):
    assert isPow2(alignment)		# non-pow2 value passed to align function
    mask = alignment - 1
    return (value + mask) & ~mask

class Long:
    def __init__(self, buffer):
        low = buffer.readUInt32()
        high = buffer.readUInt32()
        self.value = high * 4294967296.0 + low
    
    def add(self, v):
        return self.value + v.value

    def sub(self, v):
        return self.value + v.value

    def mul(self, v):
        return self.value * v.value

    def div(self, v):
        return self.value / v.value

    def less(self, v):
        return self.value < v.value

    def lessEqual(self, v):
        return self.value <= v.value

    def equal(self, v):
        return self.value == v.value
    
    def greaterEqual(self, v):
        return self.value >= v.value
    
    def greater(self, v):
        return self.value > v.value

class Time(Long):
    def __init__(self, buffer):
        Long.__init__(self, buffer)

class StringId(Long):
    pass

#########################################################################
class ConnectPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.version = buffer.readInt32()
        self.clockFrequency = buffer.readUInt64()
        self.ProcessId = buffer.readInt32()
        self.Platform = buffer.readEnum()

#------------------------------------------------------------------------
class SessionDetailsPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.padding = buffer.readInt32()
        self.name = StringId(buffer)
        self.buildId = StringId(buffer)
        self.date = StringId(buffer)

#------------------------------------------------------------------------
class TimeSpanPacket:
    def __init__(self, buffer):
        packetType_AndCore = buffer.readEnum()
        self.packetType = packetType_AndCore & 0xffff
        self.core = packetType_AndCore >> 16
        self.threadId = buffer.readInt32()
        self.nameAndSourceInfo = StringId(buffer)
        self.startTime = Time(buffer)
        self.endTime = Time(buffer)

        self.childrenTime = 0
        self.caller = null

#-------------------------------------------------------------------------
class NamedTimeSpanPacket:
    def __init__(self, buffer):
        packetType_AndCore = buffer.readEnum()
        self.packetType = packetType_AndCore & 0xffff
        self.core = packetType_AndCore >> 16
        self.threadId = buffer.readInt32()
        self.name = StringId(buffer)
        self.sourceInfo = StringId(buffer)
        self.startTime = Time(buffer)
        self.endTime = Time(buffer)

        self.childrenTime = 0
        self.caller = null

#-------------------------------------------------------------------------
class TimeSpanCustomStatPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.threadId = buffer.readInt32()
        self.valueType = buffer.readInt32()
        self.padding = buffer.readInt32()
        self.name = StringId(buffer)
        self.valueInt64 = Long(buffer)
        self.valueDouble = buffer.readDouble()
        self.time = Time(buffer)

#------------------------------------------------------------------------
class FrameStartPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.legacy1 = buffer.readInt32()
        self.legacy2 = buffer.readInt32()
        self.padding = buffer.readInt32()
        self.frameStartTime = Time(buffer)
        self.waitForSendCompleteTime = Time(buffer)
        self.legacy4 = Long(buffer)

#------------------------------------------------------------------------
class ThreadNamePacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.threadId = buffer.readUInt32()
        self.name = StringId(buffer)

#------------------------------------------------------------------------
class ThreadOrderPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.padding = buffer.readInt32()
        self.threadName = StringId(buffer)

#------------------------------------------------------------------------
class StringPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.length = buffer.readInt32()
        self.stringId = StringId(buffer)

        aligned_string_len = alignUpPow2(self.length, 4)
        self.string = buffer.readString(aligned_string_len)
        self.string = self.string.slice(0, self.length)

#------------------------------------------------------------------------
class MainThreadPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.threadId = buffer.readInt32()

#------------------------------------------------------------------------
class SessionInfoPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.threadId = buffer.readInt32()

        self.sendBufferSize = Long(buffer)
        self.stringMemorySize = Long(buffer)
        self.miscMemorySize = Long(buffer)
        self.recordingFileSize = Long(buffer)

#------------------------------------------------------------------------
class ContextSwitchPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.m_CPUId = buffer.readInt32()
        self.timestamp = Time(buffer)
        self.processId = buffer.readInt32()
        self.oldThreadId = buffer.readInt32()
        self.newThreadId = buffer.readInt32()
        self.oldThreadState = buffer.readInt32()
        self.oldThreadWaitReason = buffer.readInt32()
        self.padding = buffer.readInt32()

#------------------------------------------------------------------------
class ContextSwitchRecordingStartedPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.startedSucessfully = buffer.readInt32()
        self.error = buffer.readString(FRAMEPRO_MAX_INLINE_STRING_LENGTH)

#------------------------------------------------------------------------
class ProcessNamePacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.processId = buffer.readInt32()
        self.nameId = StringId(buffer)

#------------------------------------------------------------------------
class CustomStatPacketInt64:
    def __init__(self, buffer):
        self.packetTypeAndValueType = buffer.readEnum()
        self.count = buffer.readInt32()
        self.name = StringId(buffer)
        self.value = Long(buffer)

#------------------------------------------------------------------------
class CustomStatPacketDouble:
    def __init__(self, buffer):
        self.packetTypeAndValueType = buffer.readEnum()
        self.count = buffer.readInt32()
        self.name = StringId(buffer)
        self.value = buffer.readDouble()

#------------------------------------------------------------------------
class HiResTimer:
    def __init__(self, buffer):
        self.name = StringId(buffer)
        self.duration = Time(buffer)
        self.count = Long(buffer)

#------------------------------------------------------------------------
class HiResTimerScopePacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.padding = buffer.readInt32()
        self.startTime = Time(buffer)
        self.endTime = Time(buffer)

        self.count = buffer.readInt32()
        self.threadId = buffer.readInt32()

#------------------------------------------------------------------------
class LogPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.length = buffer.readInt32()
        self.time = Time(buffer)

#------------------------------------------------------------------------
class EventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.colour = buffer.readUInt32()
        self.name = StringId(buffer)
        self.time = Time(buffer)

#------------------------------------------------------------------------
class WaitEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.thread = buffer.readUInt32()
        self.core = buffer.readUInt32()
        self.padding = buffer.readUInt32()
        self.colour = buffer.readUInt32()
        self.eventId = Long(buffer)
        self.time = Time(buffer)

#------------------------------------------------------------------------
class CallstackPacket:
    def __init__(self, buffer):
        # we don't have a packet type here because it always follows a time span packet
        self.callstackId = buffer.readUInt32()
        self.callstackSize = buffer.readUInt32()

#------------------------------------------------------------------------
class ScopeColourPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.colour = buffer.readUInt32()
        self.name = StringId(buffer)

#------------------------------------------------------------------------
class CustomStatInfoPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.padding = buffer.readUInt32()
        self.name = StringId(buffer)
        self.value = StringId(buffer)

#------------------------------------------------------------------------
class CustomStatColourPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.colour = buffer.readUInt32()
        self.name = StringId(buffer)

#########################################################################
#------------------------------------------------------------------------
class IDSProfileCustomEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.time = Time(buffer)

        self.buffer = buffer.readBuffer(256)
        self.size = buffer.readInt32()
        self.name = StringId(buffer)

#------------------------------------------------------------------------
class IDSProfileSummaryEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.time = Time(buffer)

        self.someValueA = buffer.readUInt32()
        self.someValueB = buffer.readFloat()

#------------------------------------------------------------------------
class IDSProfileFrameDurationEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.time = Time(buffer)

        self.someValueA = buffer.readUInt32()
        self.someValueB = buffer.readFloat()

#------------------------------------------------------------------------
class IDSProfileCPUInfoEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.time = Time(buffer)

        self.someValueA = buffer.readUInt32()
        self.someValueB = buffer.readFloat()

#------------------------------------------------------------------------
class IDSProfileGPUInfoEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.time = Time(buffer)

        self.someValueA = buffer.readUInt32()
        self.someValueB = buffer.readFloat()

#------------------------------------------------------------------------
class IDSProfileSFXInfoEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.time = Time(buffer)
        self.someValueA = buffer.readUInt32()
        self.someValueB = buffer.readFloat()

#------------------------------------------------------------------------
class IDSProfileDeviceTemperatureEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.time = Time(buffer)
        self.someValueA = buffer.readUInt32()
        self.someValueB = buffer.readFloat()

#------------------------------------------------------------------------
class IDSProfileGPUShaderUpdateEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.time = Time(buffer)

        self.isUpdateEvent = buffer.readInt8()
        self.shader_id = buffer.readUInt32()
        self.shader_size = buffer.readUInt32()

        self.someValueA = buffer.readUInt32()
        self.someValueB = buffer.readFloat()

#------------------------------------------------------------------------
class IDSProfileGPUTextureUpdateEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.time = Time(buffer)

        self.isUpdateEvent = buffer.readUInt8()
        self.texture_id = buffer.readInt32()
        self.texture_format = buffer.readInt32()
        self.texture_width = buffer.readInt32()
        self.texture_height = buffer.readInt32()
        self.texture_size = buffer.readInt32()

        self.someValueA = buffer.readUInt32()
        self.someValueB = buffer.readFloat()

#------------------------------------------------------------------------
class IDSProfileGPUVertexBufferUpdateEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.time = Time(buffer)

        self.isUpdateEvent = buffer.readUInt8()
        self.vertex_buffer_id = buffer.readInt32()
        self.vertex_format = buffer.readInt32()
        self.vertex_count = buffer.readInt32()
        self.vertex_size = buffer.readInt32()

        self.someValueA = buffer.readUInt32()
        self.someValueB = buffer.readFloat()

#------------------------------------------------------------------------
class IDSProfileGPUIndexBufferUpdateEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum();
        self.time = Time(buffer);

        self.isUpdateEvent = buffer.readInt8();
        self.vertex_buffer_id = buffer.readInt32();
        self.vertex_format = buffer.readInt32();
        self.vertex_count = buffer.readInt32();
        self.vertex_size = buffer.readInt32();

        self.someValueA = buffer.readUInt32();
        self.someValueB = buffer.readFloat();

#------------------------------------------------------------------------
class IDSProfileGPUDrawCallUpdateEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum();
        self.time = Time(buffer);

        self.triangles = buffer.readInt32();

        self.someValueA = buffer.readUInt32();
        self.someValueB = buffer.readFloat();
    
#------------------------------------------------------------------------
class IDSProfileSFXBufferUpdateEventPacket:
    def __init__(self, buffer):
        self.packetType = buffer.readEnum()
        self.time = Time(buffer)

        self.isUpdateEvent = buffer.readInt8()
        self.sfx_id = buffer.readInt32()
        self.sfx_format = buffer.readInt32()
        self.sfx_size = buffer.readInt32()

        self.someValueA = buffer.readInt32()
        self.someValueB = buffer.readFloat()

#------------------------------------------------------------------------
# receive packets
#------------------------------------------------------------------------
#------------------------------------------------------------------------
class RequestStringLiteralPacket:
    def __init__(self, buffer):
        self.stringId = StringId(buffer)
        self.stringLiteralType = buffer.readUInt32()
        self.padding = buffer.readUInt32()
        self.name = StringId(buffer)

#------------------------------------------------------------------------
class SetConditionalScopeMinTimePacket:
    def __init__(self, buffer):
        self.minTime = buffer.readUInt32()

#------------------------------------------------------------------------
class ConnectResponsePacket:
    def __init__(self, buffer):
        self.interactive = buffer.readUInt32()
        self.recordContextSwitches = buffer.readUInt32()

#------------------------------------------------------------------------
class RequestRecordedDataPacket:
    def __init__(self, buffer):
        pass

#------------------------------------------------------------------------
class SetCallstackRecordingEnabledPacket:
    def __init__(self, buffer):
        self.enabled = buffer.readInt32()

#------------------------------------------------------------------------
class FrameProRecording:
    def __init__(self):
        self.buffer = null
        self.timeSpanPackets = []
        # self.stringMap = new Map();
        # self.threadNamePacketMap = new Map();