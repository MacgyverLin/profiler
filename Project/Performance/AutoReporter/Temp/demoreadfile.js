var http = require('http');
var fs = require('fs');

function functionData() {
    let b1 = Buffer.from([1, 2, 3, 4, 5, 6, 7, 8]);
    console.log(b1.readUInt8());  // logs 1  
    console.log(b1.readUInt8(0)); // logs 1  
    console.log(b1.readUInt8(1)); // logs 2  
    console.log(b1.readUInt8(7)); // logs 8  
    //console.log(b1.readUInt8(8)); // throws exception  

    let b2 = Buffer.from([1, 0, 2, 0, 3, 0, 4, 0]);
    console.log(b2.readUInt16LE());  // logs 1, read 1,0  
    console.log(b2.readUInt16LE(0)); // logs 1, read 1,0  
    console.log(b2.readUInt16LE(2)); // logs 2, read 2,0  
    console.log(b2.readUInt16LE(6)); // logs 4, read 4,0      

    let b4 = Buffer.from([1, 0, 0, 1, 0, 2]);
    console.log(b4.readUInt16LE(0)); // logs 1, read 1,0  
    console.log(b4.readUInt16BE(2)); // logs 1, read 0,1  
    console.log(b4.readUInt16BE(4)); // logs 2, read 0,2      

    let b5 = Buffer.from([0, 0, 1, 1])
    console.log(b5.slice());  // logs [0, 0, 1, 1]  
    console.log(b5.slice(0)); // logs [0, 0, 1, 1]  
    console.log(b5.slice(0, 2)); // logs [0, 0]  
    console.log(b5.slice(2, 4)); // logs [1, 1];     

    let b6 = Buffer.from([1, 0, 2, 3, 0, 0, 0, 0, 4, 5, 5, 5]);
    console.log(b6.readUInt8(0)); // logs 1  
    console.log(b6.readUInt16BE(1)); // logs 2  
    console.log(b6.readUInt16LE(3)); // logs 3  
    console.log(b6.readUInt32BE(5)); // logs 4  
    console.log(b6.slice(9, 12)); // logs [5, 5, 5]      
}

http.createServer(function (req, res) {
    fs.readFile('demofile1.html', function (err, data) {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(data);
        res.write('https://developers.google.com/chart/interactive/docs/gallery/linechart');
        //data.replace("Kim", functionData())
        functionData();
        res.end();
    });

    /*
    fs.appendFile('mynewfile1.txt', 'Hello content!', function (err) {
        if (err) throw err;
            console.log('Saved!');
    });
    
    fs.appendFile('mynewfile2.txt', 'Hello content!', function (err) {
        if (err) throw err;
            console.log('Saved!');
    });
    
    fs.unlink('mynewfile2.txt', function (err) {
        if (err) throw err;
            console.log('File deleted!');
    });
    fs.writeFile('mynewfile3.txt', 'Hello content!', function (err) {
        if (err) throw err;
        console.log('Saved!');
    });

    fs.rename('mynewfile1.txt', 'myrenamedfile.txt', function (err) {
        if (err) throw err;
        console.log('File Renamed!');
    });
    */
}).listen(8080);