var Model = require('./Model');

class TestModel extends Model {
    constructor() {
        super();

        this.length1CM = 1;
        this.length2CM = 2;
    }

    setLength1CM(value) {
        this.length1CM = value;
    }

    getLength1CM() {
        return this.length1CM;
    }

    setLength2CM(value) {
        this.length2CM = value;
    }

    getLength2CM() {
        return this.length2CM;
    }
};

module.exports = TestModel;