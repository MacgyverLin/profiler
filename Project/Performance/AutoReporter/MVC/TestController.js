var Controller = require('./Controller');
var TestModel = require('./TestModel');
var TestView = require('./TestView');

class TestController extends Controller {
    constructor(model, view) {
        super(model, view);
    }

    // get setter for model
    setLength1Inch(inch) {
        this.model.setLength1CM(this.getCM(inch));
        this.view.update(this);
    }

    getLength1Inch() {
        return getInch(this.model.getLength1CM());
    }

    setLength2Inch(inch) {
        this.model.setLength2CM(this.getCM(inch));
        this.view.update(this);
    }

    getLength2Inch() {
        return getInch(this.model.getLength2CM());
    }

    // application logic
    getAreaInch() {
        return this.model.getLength1CM() * this.model.getLength2CM() / 2.54 / 2.54;
    }

    getAreaCM() {
        return this.model.getLength1CM() * this.model.getLength2CM();
    }

    getInch(cm) {
        return cm / 2.54;
    }

    getCM(inch) {
        return inch * 2.54;
    }

    // update View
    update() {
        this.view.displayArea(this);
    }
}

module.exports = TestController;