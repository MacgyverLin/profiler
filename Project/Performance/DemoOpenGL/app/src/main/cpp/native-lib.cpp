#include <android/log.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <jni.h>
#include "FramePro.h"

class MyAllocator : public FramePro::Allocator
{
public:
    MyAllocator()
            : FramePro::Allocator()
    {
    }

    ~MyAllocator()
    {
    }

    virtual void* Alloc(size_t size)
    {
        return malloc(size);
    }

    virtual void Free(void* p)
    {
        free(p);
    }
};
MyAllocator myAllocator;
void myTestFunction()
{
    IDREAMSKY_NAMED_SCOPE("myTestFunction");
}

extern "C" JNIEXPORT void Java_com_example_opentestgl_NativeInterface_InitOpenGL(JNIEnv*env, jobject obj)
{
    IDREAMSKY_SET_ALLOCATOR(&myAllocator);
}

extern "C" JNIEXPORT void Java_com_example_opentestgl_NativeInterface_OnViewportChanged(JNIEnv*env, jobject obj,jfloat width,jfloat height)
{
    glViewport(0,0,width,height);
}

extern "C" JNIEXPORT void Java_com_example_opentestgl_NativeInterface_RenderOneFrame(JNIEnv*env, jobject obj)
{
    static float r = 0;
    r += 0.016;
    if(r>1.0)
        r -= 0.0;
    glClearColor(r, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    IDREAMSKY_START_RECORDING("/sdcard/mac.framepro_recording", false, false, 1024*1024);
    for (int i = 0; i < 3; i++)
    {
        IDREAMSKY_FRAME_START();
        IDREAMSKY_PROFILE_SUMMARY();
        IDREAMSKY_NAMED_SCOPE("Main Loop");

        // send every frame
        IDREAMSKY_PROFILE_MEMORY_INFO();
        IDREAMSKY_PROFILE_GFX_INFO();
        IDREAMSKY_PROFILE_GFX_INFO();
        IDREAMSKY_PROFILE_TEMPERATURE_POWER();
        IDREAMSKY_PROFILE_SFX_INFO();
        IDREAMSKY_PROFILE_CPU_INFO();

        myTestFunction();
    }

    IDREAMSKY_STOP_RECORDING();
}
