﻿#include "FramePro.h"
#include <iostream>
#include "Thread.h"
using namespace FramePro;
using namespace std;

void function1();
void function1_1();
void function1_2();
void function1_3();
void function1_4();
void function2();
void function2_1();
void function2_2();
void function2_3();
void function3();
void function3_1();
void function3_2();
void function3_3();
void function4();
void function4_1();
void function4_2();
void function4_3();
void function4_4();
void function5();
void function5_1();
void function5_2();
void function5_3();
void function5_4();

void function1()
{
	IDREAMSKY_NAMED_SCOPE("function1");

	Sleep(10);

	function1_1();
	function1_2();
	function1_3();
	function1_4();
}

void function1_1()
{
	IDREAMSKY_NAMED_SCOPE("function1_1");

	Sleep(20);
}

void function1_2()
{
	IDREAMSKY_NAMED_SCOPE("function1_2");

	Sleep(30);
}

void function1_3()
{
	IDREAMSKY_NAMED_SCOPE("function1_3");

	Sleep(40);
}

void function1_4()
{
	IDREAMSKY_NAMED_SCOPE("function1_4");

	Sleep(50);
}

void function2()
{
	IDREAMSKY_NAMED_SCOPE("function2");

	Sleep(60);

	function2_1();
	function2_2();
	function2_3();
}

void function2_1()
{
	IDREAMSKY_NAMED_SCOPE("function2_1");

	Sleep(70);
}

void function2_2()
{
	IDREAMSKY_NAMED_SCOPE("function2_2");

	Sleep(80);
}

void function2_3()
{
	IDREAMSKY_NAMED_SCOPE("function2_3");

	Sleep(90);
}

void function3()
{
	IDREAMSKY_NAMED_SCOPE("function3");

	Sleep(100);

	function3_1();
	function3_2();
	function3_3();
}

void function3_1_1()
{
	IDREAMSKY_NAMED_SCOPE("function3_1_1");

	Sleep(110);

	////////////////////////////////////////////////////////////////////////////////////
	// GLuint glCreateShader(GLenum shaderType​);
	// glShaderSource(GLuint shader, GLsizei count, const GLchar * const *string, const GLint *length);
	int shader_id = 0;
	int shader_size = 10;
	IDREAMSKY_PROFILE_GPU_UPDATE_SHADER(shader_id, shader_size);

	// glDeleteShader(GLuint shader);
	IDREAMSKY_PROFILE_GPU_DESTROY_SHADER(shader_id);
	
	////////////////////////////////////////////////////////////////////////////////////
	// void glGenTextures( GLsizei n, GLuint* textures);
	// void glBindTexture(GLenum target, GLuint buffer);
	// glTexImage2D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void* pixels)
	int texture_id = 0;
	int texture_format = 0;
	int texture_width = 0;
	int texture_height = 10;
	int texture_size = 10;
	IDREAMSKY_PROFILE_GPU_UPDATE_TEXTURE(texture_id, texture_format, texture_width, texture_height, texture_size);

	// void glDeleteTextures( GLsizei n, const GLuint * textures)
	IDREAMSKY_PROFILE_GPU_DESTROY_TEXTURE(texture_id);

	////////////////////////////////////////////////////////////////////////////////////
	// GLuint buffer = glGenBuffer(GLenum target);
	// void glBindBuffer(GLenum target, GLuint buffer);
	// void glBufferData(GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage);
	int vertex_buffer_id = 0;
	int vertex_format = 0;
	int vertex_count = 0;
	int vertex_size = 10;
	IDREAMSKY_PROFILE_GPU_UPDATE_VERTEX_BUFFER(vertex_buffer_id, vertex_format, vertex_count, vertex_size);

	// void glDeleteBuffers(GLsizei n,const GLuint * buffers);
	IDREAMSKY_PROFILE_GPU_DESTROY_VERTEX_BUFFER(vertex_buffer_id);

	////////////////////////////////////////////////////////////////////////////////////
	// GLuint buffer = glGenBuffer(GLenum target);
	// void glBindBuffer(GLenum target, GLuint buffer);
	// void glBufferData(GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage);
	int index_buffer_id = 0;
	int index_format = 0;
	int index_count = 0;
	int index_size = 10;
	IDREAMSKY_PROFILE_GPU_UPDATE_INDEX_BUFFER(index_buffer_id, index_format, index_count, index_size);

	// void glDeleteBuffers(GLsizei n,const GLuint * buffers);
	IDREAMSKY_PROFILE_GPU_DESTROY_INDEX_BUFFER(index_buffer_id);	

	////////////////////////////////////////////////////////////////////////////////////
	// glDrawArray(GL_TRIANGLES, index, 6);
	// glDrawElements(GL_TRIANGLES, index, 6);
	IDREAMSKY_PROFILE_GPU_DRAWCALL(6);

	////////////////////////////////////////////////////////////////////////////////////
	// IDREAMSKY_PROFILE_SFX_BUFFER(sfx_id, sfx_format, sfx_size);
}

void function3_1_2()
{
	IDREAMSKY_NAMED_SCOPE("function3_1_2");

	Sleep(110);
}

void function3_1_3()
{
	IDREAMSKY_NAMED_SCOPE("function3_1_3");

	Sleep(110);
}

void function3_1()
{
	IDREAMSKY_NAMED_SCOPE("function3_1");

	function3_1_1();
	function3_1_2();
	function3_1_3();

	Sleep(110);
}

void function3_2_1()
{
	IDREAMSKY_NAMED_SCOPE("function3_2_1");

	Sleep(110);
}

void function3_2_2()
{
	IDREAMSKY_NAMED_SCOPE("function3_2_2");

	Sleep(110);
}

void function3_2_3()
{
	IDREAMSKY_NAMED_SCOPE("function3_2_3");

	Sleep(110);
}

void function3_2()
{
	IDREAMSKY_NAMED_SCOPE("function3_2");

	function3_2_1();
	function3_2_2();
	function3_2_3();

	Sleep(120);
}

void function3_3()
{
	IDREAMSKY_NAMED_SCOPE("function3_3");

	Sleep(130);
}

void function4()
{
	IDREAMSKY_NAMED_SCOPE("function4");

	Sleep(140);

	for (int i = 0; i < 20; i++)
	{
		function4_1();
		function4_2();
		function4_3();
		function4_4();
	}
}

void function4_1()
{
	IDREAMSKY_NAMED_SCOPE("function4_1");

	Sleep(15);
}

void function4_2()
{
	IDREAMSKY_NAMED_SCOPE("function4_2");

	Sleep(20);
}

void function4_3()
{
	IDREAMSKY_NAMED_SCOPE("function4_3");

	Sleep(30);
}

void function4_4()
{
	IDREAMSKY_NAMED_SCOPE("function4_4");

	Sleep(40);
}

void function5()
{
	IDREAMSKY_NAMED_SCOPE("function5");

	Sleep(190);

	function5_1();
	function5_2();
	function5_3();
	function5_4();
}

void function5_1()
{
	IDREAMSKY_NAMED_SCOPE("function5_1");

	Sleep(200);
}

void function5_2()
{
	IDREAMSKY_NAMED_SCOPE("function5_2");

	Sleep(210);
}

void function5_3()
{
	IDREAMSKY_NAMED_SCOPE("function5_3");

	Sleep(220);
}

void function5_4()
{
	IDREAMSKY_NAMED_SCOPE("function5_4");

	Sleep(230);
}

struct MyRunnable1 : public Runnable 
{
	virtual void run()
	{
		IDREAMSKY_SET_THREAD_NAME("WorkerThread1");

		function1();
		function2();
		function3();
	}
};

struct MyRunnable2 : public Runnable
{
	virtual void run()
	{
		IDREAMSKY_SET_THREAD_NAME("WorkerThread2");
		
		function3();
		function4();
		function5();
	}
};

void myTestFunction()
{
	IDREAMSKY_NAMED_SCOPE("myTestFunction");

	Thread thread1(new MyRunnable1());
	Thread thread2(new MyRunnable2());
	thread1.start();
	thread2.start();
	thread1.join();
	thread2.join();

	//function4();
}

class MyAllocator : public FramePro::Allocator
{
public:
	MyAllocator()
	: FramePro::Allocator()
	{
	}

	~MyAllocator()
	{
	}

	virtual void* Alloc(size_t size)
	{
		return malloc(size);
	}

	virtual void Free(void* p)
	{
		free(p);
	}
};

MyAllocator myAllocator;

void test()
{
#if 1
	IDREAMSKY_SET_ALLOCATOR(&myAllocator);
	IDREAMSKY_THREAD_ORDER("Main Thread");
	IDREAMSKY_THREAD_ORDER("WorkerThread1");
	IDREAMSKY_THREAD_ORDER("WorkerThread2");

	IDREAMSKY_START_RECORDING("mac.framepro_recording", false, false, 1024*1024);


	for (int i = 0; i < 3; i++)
	//int i = 0;
	//while(1)
	{
		IDREAMSKY_FRAME_START();
		IDREAMSKY_PROFILE_SUMMARY();
		IDREAMSKY_NAMED_SCOPE("Main Loop");

		// send every frame
		IDREAMSKY_PROFILE_MEMORY_INFO();
		IDREAMSKY_PROFILE_GFX_INFO();
		IDREAMSKY_PROFILE_GFX_INFO();
		IDREAMSKY_PROFILE_TEMPERATURE_POWER();
		//IDREAMSKY_PROFILE_SFX_INFO();
		//IDREAMSKY_PROFILE_CPU_INFO();

		myTestFunction();
		printf("frame: %d\r", i);
	}

	IDREAMSKY_STOP_RECORDING();
#endif
	FrameProRecodingConverter converter;
	converter.convert("mac.framepro_recording", "mac.report");
}

int main(int argc, char** argv)
{
#if 0
	if (argc != 2)
	{
		printf("Usage: ProfilerReport inputname\n");
		return 0;
	}

	string frameProInputname = string(argv[1]);
	string inputfile = frameProInputname + ".framepro_recording";
	string reportfile = frameProInputname + ".report";
	string timereportfile = frameProInputname + "_time_report.txt";
	string callerreportfile = frameProInputname + "_caller_report.txt";

	FrameProRecodingConverter converter;
	converter.convert(inputfile.c_str(), reportfile.c_str());
	ProfilerReporter profilerReporter;
	profilerReporter.report(reportfile.c_str(), timereportfile.c_str(), callerreportfile.c_str());
#elif 0
	if (argc != 2)
	{
		printf("Usage: ProfilerReport inputname\n");
		return 0;
	}

	string frameProInputname = string(argv[1]);
	//string inputfile = frameProInputname + ".framepro_recording";
	string reportfile = frameProInputname + ".report";
	string timereportfile = frameProInputname + "_time_report.txt";
	string callerreportfile = frameProInputname + "_caller_report.txt";


	ProfilerReporter profilerReporter;
	profilerReporter.report(reportfile.c_str(), timereportfile.c_str(), callerreportfile.c_str());
#else

	// 1 android code
	test();
#endif
}