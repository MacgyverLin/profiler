#ifndef THREAD_H
#define THREAD_H

#include <windows.h>

#define CLASS_UNCOPYABLE(classname) \
private: \
classname##(const classname##&); \
classname##& operator=(const classname##&);

struct Runnable 
{
	virtual void run() = 0;
	virtual ~Runnable() {}
};

class Thread : public Runnable
{
CLASS_UNCOPYABLE(Thread)
public:
	explicit Thread(Runnable *target = 0);
	virtual ~Thread();
	virtual void run() {}
	void start();
	void join();

	static int getCurrentThreadID()
	{
#ifdef WIN32
		return GetCurrentThreadId();
#else
		// return pthread_self();
#endif
	}
private:
	static unsigned __stdcall threadProc(void *param);
private:
	Runnable *_target;
	HANDLE _handle;
};

#endif /*THREAD_H*/