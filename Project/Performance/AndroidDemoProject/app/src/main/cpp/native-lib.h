#ifndef NATIVE_LIB_H_INCLUDED
#define NATIVE_LIB_H_INCLUDED

#include <string>
#include<android/log.h>
#include "native-lib.h"

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,TAG ,__VA_ARGS__) // 定义LOGD类型

struct SummaryInfo
{
    std::string phoneName;      //手机型号
    std::string androidVersion; //android 版本号
    std::string packageName;    //包名
    std::string appName;        //应用名
    std::string vercodeName;    //版本号
    std::string cpuName;        //cpu 名称
    std::string cpuFrequency;   //cpu 频率
    std::string totalMemorySize;     //内存大小
};

bool getSummaryInfo(SummaryInfo &info);
#endif
