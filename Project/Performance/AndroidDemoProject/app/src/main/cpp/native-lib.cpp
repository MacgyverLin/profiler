#include <jni.h>
#include <string>

#include "FramePro/FramePro.h"
#include "native-lib.h"

static JNIEnv * s_env;
static SummaryInfo s_summaryInfo;

void getDeviceInfo()
{
    // 获取类
    jclass cls = NULL;
    jmethodID mid = NULL;

    JNIEnv* env = s_env;

    // 获取类和方法
    cls = env->FindClass("com/example/myapplication/MainActivity");

    mid = env->GetStaticMethodID(
            cls, "testIds", "()V");
    (env)->CallStaticVoidMethod(cls, mid);
}


extern "C" JNIEXPORT void  JNICALL Java_com_example_myapplication_MainActivity_getInfo(
        JNIEnv *env,
        jobject, /* this */
        jobjectArray oa, jfloat temp, jfloat ) {

    jsize size = env->GetArrayLength(oa);

    __android_log_print(ANDROID_LOG_DEBUG,"kim ", "######## C++ begin ##################");
    /*for(int i=0;i<size;i++)
    {
        jstring obj = (jstring)env->GetObjectArrayElement(oa,i);
        std::string sstr = (std::string)env->GetStringUTFChars(obj,NULL);//得到字符串
        __android_log_print(ANDROID_LOG_DEBUG,"kim", "########     %s",sstr.c_str());

    }

    __android_log_print(ANDROID_LOG_DEBUG,"kim ", "#################################################");*/

    if(size < 7)
        __android_log_print(ANDROID_LOG_DEBUG,"kim ", "size < 7"
                                                      "");
    //手机型号
    jstring objPhoneName = (jstring)env->GetObjectArrayElement(oa,0);
    std::string sstrPhoneName = (std::string)env->GetStringUTFChars(objPhoneName,NULL);
    s_summaryInfo.phoneName =  sstrPhoneName;
    __android_log_print(ANDROID_LOG_DEBUG,"kim", "########     %s",sstrPhoneName.c_str());

    //android 版本号
    jstring objAndroidVersion = (jstring)env->GetObjectArrayElement(oa,1);
    std::string sstrAndroidVersion = (std::string)env->GetStringUTFChars(objAndroidVersion,NULL);
    s_summaryInfo.androidVersion =  sstrAndroidVersion;
    __android_log_print(ANDROID_LOG_DEBUG,"kim", "########     %s",sstrAndroidVersion.c_str());

    //包名
    jstring objPackageName = (jstring)env->GetObjectArrayElement(oa,2);
    std::string sstrPackageName = (std::string)env->GetStringUTFChars(objPackageName,NULL);  //得到字符串
    s_summaryInfo.packageName =  sstrPackageName;
    __android_log_print(ANDROID_LOG_DEBUG,"kim", "########     %s",sstrPackageName.c_str());

    //应用名
    jstring objAppName = (jstring)env->GetObjectArrayElement(oa,3);
    std::string sstrAppName = (std::string)env->GetStringUTFChars(objAppName,NULL);
    s_summaryInfo.appName =  sstrAppName;
    __android_log_print(ANDROID_LOG_DEBUG,"kim", "########     %s",sstrAppName.c_str());

    //版本号
    jstring objVercodeName = (jstring)env->GetObjectArrayElement(oa,4);
    std::string sstrVercodeName = (std::string)env->GetStringUTFChars(objVercodeName,NULL);
    s_summaryInfo.vercodeName =  sstrVercodeName;
    __android_log_print(ANDROID_LOG_DEBUG,"kim", "########     %s",sstrVercodeName.c_str());



    //cpu 名称
    jstring objCpuName = (jstring)env->GetObjectArrayElement(oa,5);
    std::string sstrCpuName = (std::string)env->GetStringUTFChars(objCpuName,NULL);
    s_summaryInfo.cpuName =  sstrCpuName;
    __android_log_print(ANDROID_LOG_DEBUG,"kim", "########     %s",sstrCpuName.c_str());

    //cpu 频率
    jstring objCpuFrequency = (jstring)env->GetObjectArrayElement(oa,6);
    std::string sstrCpuFrequency = (std::string)env->GetStringUTFChars(objCpuFrequency,NULL);
    s_summaryInfo.cpuFrequency =  sstrCpuFrequency;
    __android_log_print(ANDROID_LOG_DEBUG,"kim", "########     %s",sstrCpuFrequency.c_str());


    //总内存大小
    jstring objTotalMemorySzie = (jstring)env->GetObjectArrayElement(oa,7);
    std::string sstrTotalMemorySize = (std::string)env->GetStringUTFChars(objTotalMemorySzie,NULL);
    s_summaryInfo.totalMemorySize =  sstrTotalMemorySize;
    __android_log_print(ANDROID_LOG_DEBUG,"kim", "########     %s",sstrTotalMemorySize.c_str());

    __android_log_print(ANDROID_LOG_DEBUG,"kim ", "######## C++ end #####################");
}

bool getSummaryInfo(SummaryInfo &info)
{
    info=s_summaryInfo;
    return 1;
}

class MyAllocator : public FramePro::Allocator
{
public:
    MyAllocator()
            : FramePro::Allocator()
    {
    }

    ~MyAllocator()
    {
    }

    virtual void* Alloc(size_t size)
    {
        return malloc(size);
    }

    virtual void Free(void* p)
    {
        free(p);
    }
};
MyAllocator myAllocator;
void myTestFunction()
{
    IDREAMSKY_NAMED_SCOPE("myTestFunction");
    __android_log_print(ANDROID_LOG_DEBUG,"kim ", "######## myTestFunction  #####################");

}

extern "C" JNIEXPORT jstring JNICALL Java_com_example_myapplication_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject ) {
    s_env=env;
    std::string hello = "Hello from C++";
    getDeviceInfo();

    IDREAMSKY_SET_ALLOCATOR(&myAllocator);


    IDREAMSKY_START_RECORDING("/sdcard/mac.framepro_recording", false, false, 1024*1024);


    for (int i = 0; i < 3; i++)
    {
        IDREAMSKY_FRAME_START();
        IDREAMSKY_NAMED_SCOPE("Main Loop");

        IDREAMSKY_PROFILE_SUMMARY();
        IDREAMSKY_PROFILE_SUMMARY();
        IDREAMSKY_PROFILE_SUMMARY();
        // send every frame
        //IDREAMSKY_PROFILE_MEMORY_INFO();
        //IDREAMSKY_PROFILE_GFX_INFO();
        //IDREAMSKY_PROFILE_GFX_INFO();
        //IDREAMSKY_PROFILE_TEMPERATURE_POWER();
        //IDREAMSKY_PROFILE_SFX_INFO();
        //IDREAMSKY_PROFILE_CPU_INFO();

        myTestFunction();
    }

    IDREAMSKY_STOP_RECORDING();
    __android_log_print(ANDROID_LOG_DEBUG,"kim ", "######## IDREAMSKY_STOP_RECORDING  #####################");

    return env->NewStringUTF(hello.c_str());
}

