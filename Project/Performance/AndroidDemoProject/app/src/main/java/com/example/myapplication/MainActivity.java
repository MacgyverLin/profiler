package com.example.myapplication;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;



public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE };

    public static MainActivity instance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 华为手机权限特别请求
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
// We don't have permission so prompt the user
            ActivityCompat.requestPermissions(this, PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE);
        }


        teleohonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        receiver = new BatteryReceiver();



        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        //Sensor TempSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        Sensor TempSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE);
        mSensorManager.registerListener(temperatureSensor, TempSensor, SensorManager.SENSOR_DELAY_FASTEST);

        instance =this;
        // Example of a call to a native method
        TextView tv = findViewById(R.id.sample_text);
        tv.setText(stringFromJNI());




    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
    public native void getInfo(String[] oa);



    //温度传感器
    private SensorEventListener temperatureSensor = new SensorEventListener(){

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            // TODO Auto-generated method stub
            float temp = event.values[0];
            Log.d("MainActivity", "sensor temp = " + temp);
        }
    };

    SensorManager mSensorManager;


    TelephonyManager teleohonyManager;
    BatteryReceiver receiver;
    ArrayList<String> listDataInfo = new ArrayList<String>();


    // 获取设备信息
    public void gtDeviceInfo() {
        Log.d("MainActivity","系统参数------------------------------begin");
        //主板
        //String board = Build.BOARD;
        //listDataInfo.add(board);
        //Log.d("MainActivity","主板：" + board);
        //系统定制商
        //String brand = Build.BRAND;
        //listDataInfo.add(brand);
        //Log.d("MainActivity","系统定制商：" + brand);
        //设备信息
        //String device = Build.DEVICE;
        //listDataInfo.add(device);
        //Log.d("MainActivity","设备信息：" + device);
        //显示屏参数
        //String display = Build.DISPLAY;
        //listDataInfo.add(display);
        //Log.d("MainActivity","显示屏参数：" + display);
        //唯一编号
        //String fingderprint = Build.FINGERPRINT;
        //listDataInfo.add(fingderprint);
        //Log.d("MainActivity","唯一编号：" + fingderprint);
        //硬件序列号
        //String serial = Build.SERIAL;
        //listDataInfo.add(serial);
        //Log.d("MainActivity","硬件序列号：" + serial);
        //修订版本列表
        //String id = Build.ID;
        //listDataInfo.add(id);
        //Log.d("MainActivity","修订版本列表：" + id);
        //硬件制造商
        String manufacturer = Build.MANUFACTURER;
        //listDataInfo.add(manufacturer);
        Log.d("MainActivity","硬件制造商：" + manufacturer);
        //版本
        //String model = Build.MODEL;
        //listDataInfo.add(model);
        //Log.d("MainActivity","版本：" + model);
        //硬件名
        //String hardware = Build.HARDWARE;
        //listDataInfo.add(hardware);
        //Log.d("MainActivity","硬件名：" + hardware);
        //手机产品名
        String product = Build.PRODUCT;
        listDataInfo.add(manufacturer+" "+product);
        Log.d("MainActivity","手机产品名：" + product);
        //描述build的标签
        //String tags = Build.TAGS;
        //listDataInfo.add(tags);
        //Log.d("MainActivity","描述build的标签：" + tags);
        //Builder类型
        //String type = Build.TYPE;
        //listDataInfo.add(type);
        //Log.d("MainActivity","Builder类型：" + type);
        //当前开发代号
        //String vcodename = Build.VERSION.CODENAME;
        //listDataInfo.add(vcodename);
        //Log.d("MainActivity","当前开发代号：" + vcodename);
        //源码控制版本号
        //String vincremental = Build.VERSION.INCREMENTAL;
        //listDataInfo.add(vincremental);
        //Log.d("MainActivity","源码控制版本号：" + vincremental);
        //版本字符串
        //String vrelease = Build.VERSION.RELEASE;
        //listDataInfo.add(vrelease);
        //Log.d("MainActivity","版本字符串：" + vrelease);
        //版本号
        int vsdkint = Build.VERSION.SDK_INT;
        listDataInfo.add(String.valueOf(vsdkint));
        Log.d("MainActivity","版本号：" + vsdkint);
        //HOST值
        //String host = Build.HOST;
        //listDataInfo.add(host);
        //Log.d("MainActivity","HOST值：" + host);
        //User名
        //String user = Build.USER;
        //listDataInfo.add(user);
        //Log.d("MainActivity","User名：" + user);
        //编译时间
        //long time = Build.TIME;
        //listDataInfo.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date(time)));
        //Log.d("MainActivity","编译时间：" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(new Date(time)));
        //OS版本号
        //String osVersion = System.getProperty("os.version");
        //listDataInfo.add(osVersion);
        //Log.d("MainActivity","OS版本号：" + osVersion);
        // OS名称
        //String osName = System.getProperty("os.name");
        //listDataInfo.add(osName);
        //Log.d("MainActivity","OS名称：" + osName);
        //OS架构
        //String osArch = System.getProperty("os.arch");
        //listDataInfo.add(osArch);
        //Log.d("MainActivity","OS架构：" + osArch);
        //home属性
        //String osUserHome = System.getProperty("os.home");
        //listDataInfo.add(osUserHome);
        //Log.d("MainActivity","home属性：" + osUserHome);
        //name属性
        //String osUserName = System.getProperty("os.name");
        //listDataInfo.add(osUserName);
        //Log.d("MainActivity","name属性 ：" + osUserName);
        //dir属性
        //String osUserDir = System.getProperty("os.dir");
        //listDataInfo.add(osUserDir);
        //Log.d("MainActivity","dir属性：" + osUserDir);
        //时区
        //String osUserTimeZone = System.getProperty("os.timezone");
        //listDataInfo.add(osUserTimeZone);
        //Log.d("MainActivity","时区：" + osUserTimeZone);
        //电话号
        //String phoneNum = teleohonyManager.getLine1Number();
        //Log.d("MainActivity","手机号：" + phoneNum);
        //集成电路卡标识
        //String iccid = teleohonyManager.getSimSerialNumber();
        //Log.d("MainActivity","集成电路卡标识：" + iccid);
        //手机电量
        //IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
       //this.registerReceiver(receiver, filter);//注册BroadcastReceiver

        //IntentFilter filterTempeature = new IntentFilter(BatteryManager.EXTRA_TEMPERATURE);
        //this.registerReceiver(receiverTemperature, filterTempeature);
        //设备id
        //String mDeviceId = teleohonyManager.getDeviceId();
        //Log.d("MainActivity","设备id：" + mDeviceId);
        //getPhoneType();
        //getLocalMacAddress();
        //getLocalIpAddress();
        //getAvailMemory();
        //getWeithAndHeight();
        //TODO 连接wifi名字
        //getCpuTemperature();
        getAppinfo();
        getCpuInfo();
        getTotalMemory();

        //IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        //this.registerReceiver(receiver, filter);//注册BroadcastReceiver


        int length=listDataInfo.size();
        String[] dataInfo=new String[length];
        for (int i = 0; i < length; i++) {
            dataInfo[i]=listDataInfo.get(i);
        }
        getInfo(dataInfo);
        listDataInfo.clear();
    }

    /**
     * 获取当前手机支持的移动网络类型
     */
    private void getPhoneType() {
        String phoneType = "";
        switch (teleohonyManager.getPhoneType()) {
            case TelephonyManager.PHONE_TYPE_NONE:
                phoneType = "NONE: ";
                break;
            case TelephonyManager.PHONE_TYPE_GSM:
                phoneType = "GSM: IMEI";
                break;
            case TelephonyManager.PHONE_TYPE_CDMA:
                phoneType = "CDMA: MEID/ESN";
                break;
            default:
                phoneType = "UNKNOWN: ID";
                break;
        }
        Log.d("MainActivity","手机网络类型：" + phoneType);
        listDataInfo.add(phoneType);
    }

    /**
     * 获取mac地址
     * 只有手机开启wifi才能获取到mac地址
     */
    public void getLocalMacAddress() {
        /*WifiManager wifi = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifi.getConnectionInfo();
        Log.d("MainActivity","MAC地址：" + info.getMacAddress());*/
    }

    /**
     * 获取IP地址
     */
    public void getLocalIpAddress() {
        /*try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        Log.d("MainActivity", "IP地址：" + inetAddress.getHostAddress().toString());
                    }
                }
            }
        } catch (SocketException ex) {
        }*/
    }



    /**
     * The default return value of any method in this class when an
     * error occurs or when processing fails (Currently set to -1). Use this to check if
     * the information about the device in question was successfully obtained.
     */
    public static final int DEVICEINFO_UNKNOWN = -1;

    /**
     * Reads the number of CPU cores from {@code /sys/devices/system/cpu/}.
     *
     * @return Number of CPU cores in the phone, or DEVICEINFO_UKNOWN = -1 in the event of an error.
     */
    public static int getNumberOfCPUCores() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
            // Gingerbread doesn't support giving a single application access to both cores, but a
            // handful of devices (Atrix 4G and Droid X2 for example) were released with a dual-core
            // chipset and Gingerbread; that can let an app in the background run without impacting
            // the foreground application. But for our purposes, it makes them single core.
            return 1;
        }
        int cores;
        try {
            cores = new File("/sys/devices/system/cpu/").listFiles(CPU_FILTER).length;
        } catch (SecurityException e) {
            cores = DEVICEINFO_UNKNOWN;
        } catch (NullPointerException e) {
            cores = DEVICEINFO_UNKNOWN;
        }
        Log.d("MainActivity","cpu核数：" + cores);
        return cores;
    }

    private static final FileFilter CPU_FILTER = new FileFilter() {
        @Override
        public boolean accept(File pathname) {
            String path = pathname.getName();
            //regex is slow, so checking char by char.
            if (path.startsWith("cpu")) {
                for (int i = 3; i < path.length(); i++) {
                    if (path.charAt(i) < '0' || path.charAt(i) > '9') {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    };

    /**
     * Method for reading the clock speed of a CPU core on the device. Will read from either
     * {@code /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq} or {@code /proc/cpuinfo}.
     *
     * @return Clock speed of a core on the device, or -1 in the event of an error.
     */
    public static int getCPUMaxFreqKHz() {
        int maxFreq = DEVICEINFO_UNKNOWN;
        try {
            int number = getNumberOfCPUCores();
            for (int i = 0; i < number; i++) {
                String filename =
                        "/sys/devices/system/cpu/cpu" + i + "/cpufreq/cpuinfo_max_freq";
                File cpuInfoMaxFreqFile = new File(filename);
                if (cpuInfoMaxFreqFile.exists()) {
                    byte[] buffer = new byte[128];
                    FileInputStream stream = new FileInputStream(cpuInfoMaxFreqFile);
                    try {
                        stream.read(buffer);
                        int endIndex = 0;
                        //Trim the first number out of the byte buffer.
                        while (buffer[endIndex] >= '0' && buffer[endIndex] <= '9'
                                && endIndex < buffer.length) endIndex++;
                        String str = new String(buffer, 0, endIndex);
                        Integer freqBound = Integer.parseInt(str);
                        if (freqBound > maxFreq) maxFreq = freqBound;
                    } catch (NumberFormatException e) {
                        //Fall through and use /proc/cpuinfo.
                    } finally {
                        stream.close();
                    }
                }
            }
            if (maxFreq == DEVICEINFO_UNKNOWN) {
                FileInputStream stream = new FileInputStream("/proc/cpuinfo");
                try {
                    int freqBound = parseFileForValue("cpu MHz", stream);
                    freqBound *= 1000; //MHz -> kHz
                    if (freqBound > maxFreq) maxFreq = freqBound;
                } finally {
                    stream.close();
                }
            }
        } catch (IOException e) {
            maxFreq = DEVICEINFO_UNKNOWN; //Fall through and return unknown.
        }
        Log.d("MainActivity","cpu最大频率：" + maxFreq);
        instance.listDataInfo.add(String.valueOf(maxFreq)+"KHz");
        return maxFreq;
    }


    /**
     * Helper method for reading values from system files, using a minimised buffer.
     *
     * @param textToMatch - Text in the system files to read for.
     * @param stream      - FileInputStream of the system file being read from.
     * @return A numerical value following textToMatch in specified the system file.
     * -1 in the event of a failure.
     */
    private static int parseFileForValue(String textToMatch, FileInputStream stream) {
        byte[] buffer = new byte[1024];
        try {
            int length = stream.read(buffer);
            for (int i = 0; i < length; i++) {
                if (buffer[i] == '\n' || i == 0) {
                    if (buffer[i] == '\n') i++;
                    for (int j = i; j < length; j++) {
                        int textIndex = j - i;
                        //Text doesn't match query at some point.
                        if (buffer[j] != textToMatch.charAt(textIndex)) {
                            break;
                        }
                        //Text matches query here.
                        if (textIndex == textToMatch.length() - 1) {
                            return extractValue(buffer, j);
                        }
                    }
                }
            }
        } catch (IOException e) {
            //Ignore any exceptions and fall through to return unknown value.
        } catch (NumberFormatException e) {
        }
        return DEVICEINFO_UNKNOWN;
    }

    /**
            * Helper method used by {@link #parseFileForValue(String, FileInputStream) parseFileForValue}. Parses
   * the next available number after the match in the file being read and returns it as an integer.
            * @param index - The index in the buffer array to begin looking.
   * @return The next number on that line in the buffer, returned as an int. Returns
   * DEVICEINFO_UNKNOWN = -1 in the event that no more numbers exist on the same line.
            */
    private static int extractValue(byte[] buffer, int index) {
        while (index < buffer.length && buffer[index] != '\n') {
            if (buffer[index] >= '0' && buffer[index] <= '9') {
                int start = index;
                index++;
                while (index < buffer.length && buffer[index] >= '0' && buffer[index] <= '9') {
                    index++;
                }
                String str = new String(buffer, 0, start, index - start);
                return Integer.parseInt(str);
            }
            index++;
        }
        return DEVICEINFO_UNKNOWN;
    }


    /**
     * 手机CPU信息
     */
    private void getCpuInfo() {
        String str1 = "/proc/cpuinfo";
        String str2 = "";
        String[] cpuInfo = {"", ""};  //1-cpu型号  //2-cpu频率
        String[] arrayOfString;
        try {
            FileReader fr = new FileReader(str1);
            BufferedReader localBufferedReader = new BufferedReader(fr, 8192);
            str2 = localBufferedReader.readLine();
            arrayOfString = str2.split("\\s+");
            for (int i = 2; i < arrayOfString.length; i++) {
                cpuInfo[0] = cpuInfo[0] + arrayOfString[i] + " ";
            }
            str2 = localBufferedReader.readLine();
            Log.d("MainActivity","cpuInfo：" + str2);
            arrayOfString = str2.split("\\s+");
            cpuInfo[1] += arrayOfString[2];
            localBufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Log.d("MainActivity","cpu型号：" + cpuInfo[0] + "cpu频率：" + cpuInfo[1]);
        Log.d("MainActivity","cpu型号：" + cpuInfo[0] );
        listDataInfo.add(cpuInfo[0]);



        getCPUMaxFreqKHz();
    }

    /**
     * 获取当前可用内存大小
     */
    private void getTotalMemory() {
        ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        am.getMemoryInfo(mi);
        //mi.availMem; 当前系统的可用内存
        //Log.d("MainActivity","可用运行内存大小：" + Formatter.formatFileSize(this, mi.availMem));
        Log.d("MainActivity","内存大小：" + Formatter.formatFileSize(this, mi.totalMem));
        listDataInfo.add(Formatter.formatFileSize(this, mi.totalMem));
    }

    /**
     * 获得系统总内存
     */
    /*private void getTotalMemory() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        Log.d("MainActivity","总内存：" + Formatter.formatFileSize(this, blockSize * totalBlocks));
        listDataInfo.add(Formatter.formatFileSize(this, blockSize * totalBlocks));
    }*/


    /**
     * 获取屏幕宽高
     */
    private void getWeithAndHeight() {
        /*WindowManager mWindowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        int width = mWindowManager.getDefaultDisplay().getWidth();
        int height = mWindowManager.getDefaultDisplay().getHeight();
        SLog.Console("屏幕尺寸：" + width + "x" + height);*/
    }

    private class BatteryReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {
                int current = intent.getExtras().getInt("level");//获得当前电量
                int total = intent.getExtras().getInt("scale");//获得总电量
                int percent = current * 100 / total;
                Log.d("MainActivity","电量：" + percent + "%。");
                listDataInfo.add(String.valueOf(percent)+"%");

                float currentBatteryTemperature = (((float) intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1))) / 10f;
                Log.d("MainActivity","温度：" + currentBatteryTemperature);
                listDataInfo.add(String.valueOf(currentBatteryTemperature));

                context.unregisterReceiver(receiver);


                int length=listDataInfo.size();
                String[] dataInfo=new String[length];
                for (int i = 0; i < length; i++) {
                    //System.out.println(listDataInfo.get(i));
                    dataInfo[i]=listDataInfo.get(i);
                }

                getInfo(dataInfo);

                listDataInfo.clear();
            }
        }
    }

   private void getCpuTemperature()
   {
       String text4 = "0";
       try {
           //FileReader fr4 = new FileReader("/sys/class/thermal/thermal_zone9/subsystem/thermal_zone9/temp");
           FileReader fr4 = new FileReader("/sys/devices/virtual/thermal/thermal_zone1/temp");
           BufferedReader br4 = new BufferedReader(fr4);
           text4 = br4.readLine();
           br4.close();
       } catch (IOException e) {
           // TODO 自动生成的 catch 块
       }
       //TextView p_temp = new TextView(getActivity());
      // p_temp.setTextColor(Color.BLACK);
      // p_temp.setText("CPU温度：" + text4 + "℃");
       Log.d("MainActivity","CPU温度：" + text4 + "℃");
   }

    //获取App信息
    private void getAppinfo() {

        try {
            PackageManager packageManager = getPackageManager();

            PackageInfo info = packageManager.getPackageInfo(getPackageName(), 0);

            String packageName = info.packageName;
            listDataInfo.add(packageName);
            Log.d("MainActivity","packageName：" + packageName);

            ApplicationInfo applicationInfo = info.applicationInfo;
            String appName = (String) applicationInfo.loadLabel(packageManager);
            listDataInfo.add(appName);
            Log.d("MainActivity","appName：" + appName);

            String name = info.versionName;
            Log.d("MainActivity","versionName：" + name);
            listDataInfo.add(name);

            //int code = info.versionCode;
            //listDataInfo.add(String.valueOf(code));
            //Log.d("MainActivity","versionCode：" + code);





        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static void testIds()
    {
        Log.d("MainActivity","testIds:" );
        instance.gtDeviceInfo();
    }




}
