class FileBuffer {
    constructor(buffer) {
        this.buffer = buffer;
        this.offset = 0;
    }

    readUInt8() {
        let val = this.peekUInt8();
        this.offset += 1;
        return val;
    }

    readUInt16() {
        let val = this.peekUInt16();
        this.offset += 2;
        return val;
    }

    readUInt32() {
        let val = this.peekUInt32();
        this.offset += 4;
        return val;
    }

    readUInt64() {
        let val = this.peekUInt64();
        this.offset += 8;

        return val;
    }

    readInt8() {
        let val = this.peekInt8();
        this.offset += 1;
        return val;
    }

    readInt16() {
        let val = this.peekInt16();
        this.offset += 2;
        return val;
    }

    readInt32() {
        let val = this.peekInt32();
        this.offset += 4;
        return val;
    }

    readInt64() {
        let val = this.peekInt64();
        this.offset += 8;

        return val;
    }

    readFloat() {
        let val = this.peekFloat();
        this.offset += 4;
        return val;
    }

    readDouble() {
        let val = this.peekDouble();
        this.offset += 8;
        return val;
    }

    readEnum() {
        return this.readUInt32();
    }

    readString(size) {
        let val = this.peekString(size);
        this.offset += size;
        return val;
    }

    readBuffer(size) {
        let val = this.peekBuffer(size);
        this.offset += size;
        return val;
    }

    peekUInt8() {
        let val = this.buffer.readUInt8(this.offset);
        return val;
    }

    peekUInt16() {
        let val = this.buffer.readUInt16LE(this.offset);
        return val;
    }

    peekUInt32() {
        let val = this.buffer.readUInt32LE(this.offset);
        return val;
    }

    peekUInt64() {
        let val = [this.buffer.readUInt32LE(this.offset), this.buffer.readUInt32LE(this.offset + 4)];
        return val;
    }

    peekInt8() {
        let val = this.buffer.readInt8(this.offset);
        return val;
    }

    peekInt16() {
        let val = this.buffer.readInt16LE(this.offset);
        return val;
    }

    peekInt32() {
        let val = this.buffer.readInt32LE(this.offset);
        return val;
    }

    peekInt64() {
        let val = [this.buffer.readInt32LE(this.offset), this.buffer.readInt32LE(this.offset + 4)];
        return val;
    }

    peekFloat() {
        let val = this.buffer.readFloatLE(this.offset);
        return val;
    }

    peekDouble() {
        let val = this.buffer.readDoubleLE(this.offset);
        return val;
    }

    peekEnum() {
        return this.peekUInt32();
    }

    peekString(size) {
        let val = this.buffer.toString('ascii', this.offset, this.offset + size)
        return val;
    }

    peekBuffer(size) {
        return this.peekString(size);
    }

    length() {
        return this.buffer.length;
    }

    eof() {
        return this.offset >= this.buffer.length;
    }
}

module.exports = FileBuffer;