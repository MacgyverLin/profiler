const assert = require("assert");
const FileBuffer = require("./FileBuffer");
const Model = require("./MVC/Model");

var PacketType =
{
    Connect: 0xaabb,
    FrameStart: 0xaabc,
    TimeSpan: 0xaabd,
    TimeSpanW: 0xaabe,
    NamedTimeSpan: 0xaabf,
    StringLiteralNamedTimeSpan: 0xaac0,
    ThreadName: 0xaac1,
    ThreadOrder: 0xaac2,
    StringPacket: 0xaac3,
    WStringPacket: 0xaac4,
    NameAndSourceInfoPacket: 0xaac5,
    NameAndSourceInfoPacketW: 0xaac6,
    SourceInfoPacket: 0xaac7,
    MainThreadPacket: 0xaac8,
    RequestStringLiteralPacket: 0xaac9,
    SetConditionalScopeMinTimePacket: 0xaaca,
    ConnectResponsePacket: 0xaacb,
    SessionInfoPacket: 0xaacc,
    RequestRecordedDataPacket: 0xaacd,
    SessionDetailsPacket: 0xaace,
    ContextSwitchPacket: 0xaacf,
    ContextSwitchRecordingStartedPacket: 0xaad0,
    ProcessNamePacket: 0xaad1,
    CustomStatPacket_Depreciated: 0xaad2,
    StringLiteralTimerNamePacket: 0xaad3,
    HiResTimerScopePacket: 0xaad4,
    LogPacket: 0xaad5,
    EventPacket: 0xaad6,
    StartWaitEventPacket: 0xaad7,
    StopWaitEventPacket: 0xaad8,
    TriggerWaitEventPacket: 0xaad9,
    TimeSpanCustomStatPacket_Depreciated: 0xaada,
    TimeSpanWithCallstack: 0xaadb,
    TimeSpanWWithCallstack: 0xaadc,
    NamedTimeSpanWithCallstack: 0xaadd,
    StringLiteralNamedTimeSpanWithCallstack: 0xaade,
    ModulePacket: 0xaadf,
    SetCallstackRecordingEnabledPacket: 0xaae0,
    CustomStatPacketW: 0xaae1,
    TimeSpanCustomStatPacketW: 0xaae2,
    CustomStatPacket: 0xaae3,
    TimeSpanCustomStatPacket: 0xaae4,
    ScopeColourPacket: 0xaae5,
    CustomStatColourPacket: 0xaae6,
    CustomStatGraphPacket: 0xaae7,
    CustomStatUnitPacket: 0xaae8,
    BufferEventPacket: 0xaae9
};

// enum
var Platform = {
    Windows: 0,
    Windows_UWP: 1,
    XBoxOne: 2,
    Unused: 3,
    Linux: 4,
    PS4: 5,
    Android: 6,
    Mac: 7,
    iOS: 8,
    Switch: 9
}

const FRAMEPRO_MAX_INLINE_STRING_LENGTH = 256;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function isPow2(value) {
    return (value & (value - 1)) == 0;
}

function alignUpPow2(value, alignment) {
    assert(isPow2(alignment));		// non-pow2 value passed to align function

    mask = alignment - 1;

    return (value + mask) & ~mask;
}

class Long {
    constructor(buffer) {
        var low = buffer.readUInt32();
        var high = buffer.readUInt32();

        this.value = high * 4294967296.0 + low;
    }

    add(v) {
        return this.value + v.value;
    }

    sub(v) {
        return this.value - v.value;
    }

    mul(v) {
        return this.value * v.value;
    }

    div(v) {
        return this.value / v.value;
    }

    less(v) {
        return this.value < v.value;
    }

    lessEqual(v) {
        return this.value <= v.value;
    }

    equal(v) {
        return this.value == v.value;
    }

    greaterEqual(v) {
        return this.value >= v.value;
    }

    greater(v) {
        return this.value > v.value;
    }
}

class Time extends Long {
    constructor(buffer) {
        super(buffer);
    }
}

class StringId extends Long {
    constructor(buffer) {
        super(buffer);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class ConnectPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.version = buffer.readInt32();
        this.clockFrequency = buffer.readUInt64();
        this.m_ProcessId = buffer.readInt32();
        this.m_Platform = buffer.readEnum();
    }
}

//------------------------------------------------------------------------
class SessionDetailsPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.padding = buffer.readInt32();
        this.name = new StringId(buffer);
        this.buildId = new StringId(buffer);
        this.date = new StringId(buffer);
    }
}

//------------------------------------------------------------------------
class TimeSpanPacket {
    constructor(buffer) {
        var packetType_AndCore = buffer.readEnum();
        this.packetType = packetType_AndCore & 0xffff;
        this.core = packetType_AndCore >>> 16;
        this.threadId = buffer.readInt32();
        this.nameAndSourceInfo = new StringId(buffer);
        this.startTime = new Time(buffer);
        this.endTime = new Time(buffer);

        this.childrenTime = 0;
        this.caller = null;
    };

    static checkCaller(timespan1, timespan2) {
        var c1 = (timespan1.startTime.less(timespan2.startTime)) && (timespan2.startTime.less(timespan1.endTime));
        var c2 = (timespan1.startTime.less(timespan2.endTime)) && (timespan2.endTime.less(timespan1.endTime));
        var c3 = (timespan1.threadId == timespan2.threadId);

        return c1 && c2 && c3;
    }

    isSameThread(other) {
        return this.threadId <= other.threadId;
    }

    getTotalTime() {
        return this.endTime.sub(this.startTime);
    }

    getSelfTime() {
        return this.getTotalTime() - this.childrenTime;
    }

    setCaller(caller) {
        this.caller = caller;

        if (this.caller)
            this.caller.childrenTime += this.getTotalTime();
    }

    getCaller() {
        return this.caller;
    }

    isCallerOf(other) {
        return other.caller == this;
    }
}

//------------------------------------------------------------------------
class NamedTimeSpanPacket {
    constructor(buffer) {
        var packetType_AndCore = buffer.readEnum();
        this.packetType = packetType_AndCore & 0xffff;
        this.core = packetType_AndCore >>> 16;
        this.threadId = buffer.readInt32();
        this.name = new StringId(buffer);
        this.sourceInfo = new StringId(buffer);
        this.startTime = new Time(buffer);
        this.endTime = new Time(buffer);

        this.childrenTime = 0;
        this.caller = null;
    }

    static checkCaller(timespan1, timespan2) {
        var c1 = (timespan1.startTime.less(timespan2.startTime)) && (timespan2.startTime.less(timespan1.endTime));
        var c2 = (timespan1.startTime.less(timespan2.endTime)) && (timespan2.endTime.less(timespan1.endTime));
        var c3 = (timespan1.threadId == timespan2.threadId);

        return c1 && c2 && c3;
    }

    isSameThread(other) {
        return this.threadId <= other.threadId;
    }

    getTotalTime() {
        return this.endTime.sub(this.startTime);
    }

    getSelfTime() {
        return this.getTotalTime() - this.childrenTime;
    }

    setCaller(caller) {
        this.caller = caller;

        if (this.caller)
            this.caller.childrenTime += this.getTotalTime();
    }

    getCaller() {
        return this.caller;
    }

    isCallerOf(other) {
        return other.caller == this;
    }
}

//------------------------------------------------------------------------
class TimeSpanCustomStatPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.threadId = buffer.readInt32();
        this.valueType = buffer.readInt32();
        this.padding = buffer.readInt32();
        this.name = new StringId(buffer);
        this.valueInt64 = new Long(buffer);
        this.valueDouble = buffer.readDouble();
        this.time = new Time(buffer);
    }
}

//------------------------------------------------------------------------
class FrameStartPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.legacy1 = buffer.readInt32();
        this.legacy2 = buffer.readInt32();
        this.padding = buffer.readInt32();
        this.frameStartTime = new Time(buffer);
        this.waitForSendCompleteTime = new Time(buffer);
        this.legacy4 = new Long(buffer);
    }
}

//------------------------------------------------------------------------
class ThreadNamePacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.threadId = buffer.readUInt32();
        this.name = new StringId(buffer);
    }
}

//------------------------------------------------------------------------
class ThreadOrderPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.padding = buffer.readInt32();
        this.threadName = new StringId(buffer);
    }
}

//------------------------------------------------------------------------
class StringPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.length = buffer.readInt32();
        this.stringId = new StringId(buffer);

        var aligned_string_len = alignUpPow2(this.length, 4);
        this.string = buffer.readString(aligned_string_len);//
        this.string = this.string.slice(0, this.length);
    }
}

//------------------------------------------------------------------------
class MainThreadPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.threadId = buffer.readInt32();
    }
}

//------------------------------------------------------------------------
class SessionInfoPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.threadId = buffer.readInt32();

        this.sendBufferSize = new Long(buffer);
        this.stringMemorySize = new Long(buffer);
        this.miscMemorySize = new Long(buffer);
        this.recordingFileSize = new Long(buffer);
    }
}

//------------------------------------------------------------------------
class ContextSwitchPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.m_CPUId = buffer.readInt32();
        this.timestamp = new Time(buffer);
        this.processId = buffer.readInt32();
        this.oldThreadId = buffer.readInt32();
        this.newThreadId = buffer.readInt32();
        this.oldThreadState = buffer.readInt32();
        this.oldThreadWaitReason = buffer.readInt32();
        this.padding = buffer.readInt32();
    };
}

//------------------------------------------------------------------------
class ContextSwitchRecordingStartedPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.startedSucessfully = buffer.readInt32();
        this.error = buffer.readString(FRAMEPRO_MAX_INLINE_STRING_LENGTH);
    };
};

//------------------------------------------------------------------------
class ProcessNamePacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.processId = buffer.readInt32();
        this.nameId = new StringId(buffer);
    };
};

//------------------------------------------------------------------------
class CustomStatPacketInt64 {
    constructor(buffer) {
        this.packetTypeAndValueType = buffer.readEnum();
        this.count = buffer.readInt32();
        this.name = new StringId(buffer);
        this.value = new Long(buffer);
    };
};

//------------------------------------------------------------------------
class CustomStatPacketDouble {
    constructor(buffer) {
        this.packetTypeAndValueType = buffer.readEnum();
        this.count = buffer.readInt32();
        this.name = new StringId(buffer);
        this.value = buffer.readDouble();
    };
};

//------------------------------------------------------------------------
class HiResTimer {
    constructor(buffer) {
        this.name = new StringId(buffer);
        this.duration = new Time(buffer);
        this.count = new Long(buffer);
    };
};

class HiResTimerScopePacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.padding = buffer.readInt32();
        this.startTime = new Time(buffer);
        this.endTime = new Time(buffer);

        this.count = buffer.readInt32();
        this.threadId = buffer.readInt32();
    };
};

//------------------------------------------------------------------------
class LogPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.length = buffer.readInt32();
        this.time = new Time(buffer);
    };
};

//------------------------------------------------------------------------
class EventPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.colour = buffer.readUInt32();
        this.name = new StringId(buffer);
        this.time = new Time(buffer);
    };
};

//------------------------------------------------------------------------
class WaitEventPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.thread = buffer.readUInt32();
        this.core = buffer.readUInt32();
        this.padding = buffer.readUInt32();
        this.colour = buffer.readUInt32();
        this.eventId = new Long(buffer);
        this.time = new Time(buffer);
    };
};

//------------------------------------------------------------------------
class CallstackPacket {
    constructor(buffer) {
        // we don't have a packet type here because it always follows a time span packet
        this.callstackId = buffer.readUInt32();
        this.callstackSize = buffer.readUInt32();
    };
};

//------------------------------------------------------------------------
class ScopeColourPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.colour = buffer.readUInt32();
        this.name = new StringId(buffer);
    };
};

//------------------------------------------------------------------------
class CustomStatInfoPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.padding = buffer.readUInt32();
        this.name = new StringId(buffer);
        this.value = new StringId(buffer);
    };
};

//------------------------------------------------------------------------
class CustomStatColourPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.colour = buffer.readUInt32();
        this.name = new StringId(buffer);
    };
};

class BufferEventPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.buffer = buffer.readBuffer(256);
        this.size = buffer.readInt32();
        this.name = new StringId(buffer);
        this.time = new Time(buffer);
    };
};

//------------------------------------------------------------------------
// receive packets
//------------------------------------------------------------------------
//------------------------------------------------------------------------
class RequestStringLiteralPacket {
    constructor(buffer) {
        this.stringId = new StringId(buffer);
        this.stringLiteralType = buffer.readUInt32();
        this.padding = buffer.readUInt32();
        this.name = new StringId(buffer);
    };
};

//------------------------------------------------------------------------
class SetConditionalScopeMinTimePacket {
    constructor(buffer) {
        this.minTime = buffer.readUInt32();
    };
};

//------------------------------------------------------------------------
class ConnectResponsePacket {
    constructor(buffer) {
        this.interactive = buffer.readUInt32();
        this.recordContextSwitches = buffer.readUInt32();
    };
};

//------------------------------------------------------------------------
class RequestRecordedDataPacket {
    constructor(buffer) {
    };
};

//------------------------------------------------------------------------
class SetCallstackRecordingEnabledPacket {
    constructor(buffer) {
        this.enabled = buffer.readInt32();
    };
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class TimeProfileModel extends Model {
    constructor() {
        super();

        this.buffer = null;
        this.timeSpanPackets = [];
        this.stringMap = new Map();
        this.threadNamePacketMap = new Map();
    }

    parseTimeSpanPacketCore() {
        var timeSpanPacket = new TimeSpanPacket(this.buffer);

        this.timeSpanPackets.push(timeSpanPacket);

        return timeSpanPacket;
    }

    parseNamedTimeSpanPacketCore() {
        assert(false, "not supported");

        var namedTimeSpanPacket = new NamedTimeSpanPacket(this.buffer);

        this.namedTimeSpanPackets.push(namedTimeSpanPacket);

        return namedTimeSpanPacket;
    }

    parseStringCore() {
        var stringPacket = new StringPacket(this.buffer);

        this.stringMap.set(stringPacket.stringId.value, stringPacket.string);
        return stringPacket;
    }

    parseHeader() {
        let header = this.buffer.readString(18);
        return header;
    }

    parseConnect() {
        this.connectPacket = new ConnectPacket(this.buffer);

        console.log("parseConnect");
        // onConnectPacket(this.connectPacket);
    }

    parseFrameStart() {
        this.frameStartPacket = new FrameStartPacket(this.buffer);

        console.log("parseFrameStart");
        // onFrameStartPacket(frameStartPacket);
    }

    parseTimeSpan() {
        var timeSpanPacket = this.parseTimeSpanPacketCore();

        //console.log("parseTimeSpan");
        // onTimeSpan(timeSpanPacket);
    }

    parseTimeSpanW() {
        var timeSpanPacketW = this.parseTimeSpanPacketCore();

        //console.log("parseTimeSpanW");
        // onTimeSpanW(timeSpanPacketW);
    }

    parseNamedTimeSpan() {
        var namedTimeSpanPacketCore = this.parseNamedTimeSpanPacketCore();

        //console.log("parseNamedTimeSpan");
        // onNamedTimeSpan(namedTimeSpanPacketCore);
    }

    parseStringLiteralNamedTimeSpan() {
        var namedTimeSpanPacketCore = this.parseNamedTimeSpanPacketCore();

        //console.log("parseStringLiteralNamedTimeSpan");
        // onStringLiteralNamedTimeSpan(namedTimeSpanPacketCore);
    }

    parseThreadName() {
        var threadNamePacket = new ThreadNamePacket(this.buffer);
        this.threadNamePacketMap.set(threadNamePacket.threadId, threadNamePacket);

        // console.log("parseThreadName");
        // onThreadName(threadNamePacket);
    }

    parseThreadOrder() {
        this.threadOrderPacket = new ThreadOrderPacket(this.buffer);

        console.log("parseThreadOrder");
        // onThreadOrder(threadOrderPacket);
    }

    parseStringPacket() {
        var stringPacket = this.parseStringCore();

        console.log("parseStringPacket");
        // onStringPacket(this.stringPacket);
    }

    parseWStringPacket() {
        var stringPacket = this.parseStringCore();

        console.log("parseWStringPacket");
        // onWStringPacket(this.stringPacket);
    }

    parseNameAndSourceInfoPacket() {
        var stringPacket = this.parseStringCore();

        console.log("parseNameAndSourceInfoPacket");
        // onNameAndSourceInfoPacket(this.stringPacket);
    }

    parseNameAndSourceInfoPacketW() {
        var stringPacket = this.parseStringCore();

        console.log("parseNameAndSourceInfoPacketW");
        // onNameAndSourceInfoPacketW(this.stringPacket);
    }

    parsereadSourceInfoPacket() {
        var stringPacket = this.parseStringCore();

        console.log("parseSourceInfoPacket");
        // onSourceInfoPacket(this.stringPacket);
    }

    parseMainThreadPacket() {
        this.mainThreadPacket = new MainThreadPacket(this.buffer);

        console.log("parseMainThreadPacket");
        // onMainThreadPacket(this.mainThreadPacket);
    }

    parseRequestStringLiteralPacket() {
        this.requestStringLiteralPacket = new RequestStringLiteralPacket(this.buffer);

        console.log("parseRequestStringLiteralPacket");
        // onRequestStringLiteralPacket(this.requestStringLiteralPacket);
    }

    parseSetConditionalScopeMinTimePacket() {
        this.setConditionalScopeMinTimePacket = new SetConditionalScopeMinTimePacket(this.buffer);

        console.log("parseSetConditionalScopeMinTimePacket");
        // onSetConditionalScopeMinTimePacket(this.setConditionalScopeMinTimePacket);
    }

    parseConnectResponsePacket() {
        this.connectResponsePacket = new ConnectResponsePacket(this.buffer);

        console.log("parseConnectResponsePacket");
        // onConnectResponsePacket(this.readConnectResponsePacket);
    }

    parseSessionInfoPacket() {
        this.sessionInfoPacket = new SessionInfoPacket(this.buffer);

        console.log("parseSessionInfoPacket");
        // onSessionInfoPacket(this.readSessionInfoPacket);
    }

    parseRequestRecordedDataPacket() {
        this.requestRecordedDataPacket = new RequestRecordedDataPacket(this.buffer);

        console.log("parseRequestRecordedDataPacket");
        // onRequestRecordedDataPacket(this.requestRecordedDataPacket);
    }

    parseSessionDetailsPacket() {
        this.sessionDetailsPacket = new SessionDetailsPacket(this.buffer);

        console.log("parseSessionDetailsPacket");
        // onSessionDetailsPacket(this.sessionDetailsPacket);
    }

    parseContextSwitchPacket() {
        this.contextSwitchPacket = new ContextSwitchPacket(this.buffer);

        console.log("parseContextSwitchPacket");
        // onContextSwitchPacket(this.contextSwitchPacket);
    }

    parseContextSwitchRecordingStartedPacket() {
        this.contextSwitchRecordingStartedPacket = new ContextSwitchRecordingStartedPacket(this.buffer);

        console.log("parseContextSwitchRecordingStartedPacket");
        // onContextSwitchRecordingStartedPacket(this.contextSwitchRecordingStartedPacket);
    }

    parseProcessNamePacket() {
        this.processNamePacket = new ProcessNamePacket(this.buffer);

        console.log("parseProcessNamePacket");
        // onProcessNamePacket(this.processNamePacket);
    }

    parseCustomStatPacket_Depreciated() {
        console.log("parseCustomStatPacket_Depreciated");
        // onCustomStatPacket_Depreciated();
    }

    parseStringLiteralTimerNamePacket() {
        var stringPacket = this.readStringCore();

        console.log("parseStringLiteralTimerNamePacket");
        // onStringLiteralTimerNamePacket(stringPacket);
    }

    parseHiResTimerScopePacket() {
        this.hiResTimerScopePacket = new HiResTimerScopePacket(this.buffer);

        console.log("parseHiResTimerScopePacket");
        // onHiResTimerScopePacket(this.hiResTimerScopePacket);
    }

    parseLogPacket() {
        this.logPacket = new LogPacket(this.buffer);

        console.log("parseLogPacket");
        // onLogPacket(this.logPacket);
    }

    parseEventPacket() {
        this.eventPacket = new EventPacket(this.buffer);

        console.log("parseEventPacket");
        // onEventPacket(this.eventPacket);
    }

    parseStartWaitEventPacket() {
        this.startWaitEventPacket = new WaitEventPacket(this.buffer);

        console.log("parseStartWaitEventPacket");
        // onStartWaitEventPacket(this.startWaitEventPacket);
    }

    parseStopWaitEventPacket() {
        this.stopWaitEventPacket = new WaitEventPacket(this.buffer);

        console.log("parseStopWaitEventPacket");
        // onStopWaitEventPacket(this.stopWaitEventPacket);
    }

    parseTriggerWaitEventPacket() {
        this.triggerWaitEventPacket = new WaitEventPacket(this.buffer);

        console.log("parseStopWaitEventPacket");
        // onStopWaitEventPacket(this.stopWaitEventPacket);
    }

    parseTimeSpanCustomStatPacket_Depreciated() {
        console.log("parseTimeSpanCustomStatPacket_Depreciated");
        // onTimeSpanCustomStatPacket_Depreciated();
    }

    parseTimeSpanWithCallstack() {
        this.timeSpanPacket = new TimeSpanPacket(this.buffer);
        this.callstackPacket = new CallstackPacket(this.buffer);

        if (this.callstackPacket.callstackSize)
            buffer.readString(this.callstackPacket.callstackSize);

        console.log("parseTimeSpanWithCallstack");
        //onTimeSpanWithCallstack(timeSpanPacket, callstackPacket);
    }

    parseTimeSpanWWithCallstack() {
        this.timeSpanPacket = new TimeSpanPacket(this.buffer);
        this.callstackPacket = new CallstackPacket(this.buffer);

        if (this.callstackPacket.callstackSize)
            buffer.readString(this.callstackPacket.callstackSize);

        console.log("parseTimeSpanWWithCallstack");
        //onTimeSpanWWithCallstack(timeSpanPacket, callstackPacket);
    }

    parseNamedTimeSpanWithCallstack() {
        let namedTimeSpanPacket = new NamedTimeSpanPacket(this.buffer);
        let callstackPacket = new CallstackPacket(this.buffer);

        if (this.callstackPacket.callstackSize)
            buffer.readString(this.callstackPacket.callstackSize);

        console.log("parseNamedTimeSpanWithCallstack");
        //onNamedTimeSpanWithCallstack(namedTimeSpanPacket, callstackPacket);
    }

    parseStringLiteralNamedTimeSpanWithCallstack() {
        let namedTimeSpanPacket = new NamedTimeSpanPacket(this.buffer);
        let callstackPacket = new CallstackPacket(this.buffer);

        if (this.callstackPacket.callstackSize)
            buffer.readString(this.callstackPacket.callstackSize);

        console.log("parseStringLiteralNamedTimeSpanWithCallstack");
        //onStringLiteralNamedTimeSpanWithCallstack(namedTimeSpanPacket, callstackPacket);
    }

    parseModulePacket() {
        let modulePacket = new ModulePacket(this.buffer);

        console.log("parseModulePacket");
        //onModulePacket(modulePacket);
    }

    parseSetCallstackRecordingEnabledPacket() {
        let setCallstackRecordingEnabledPacket = new SetCallstackRecordingEnabledPacket(this.buffer);

        console.log("parseSetCallstackRecordingEnabledPacket");
        //onSetCallstackRecordingEnabledPacket(setCallstackRecordingEnabledPacket);
    }

    parseCustomStatPacketW() {
        let customStatPacketW = new CustomStatPacketW(this.buffer);

        console.log("parseCustomStatPacketW");
        //onCustomStatPacketW(customStatPacketW);
    }

    parseTimeSpanCustomStatPacketW() {
        let timeSpanCustomStatPacketW = new TimeSpanCustomStatPacketW(this.buffer);

        console.log("parseTimeSpanCustomStatPacketW");
        //onTimeSpanCustomStatPacketW(timeSpanCustomStatPacketW);
    }

    parseCustomStatPacket() {
        let customStatPacket = new CustomStatPacket(this.buffer);

        console.log("parseCustomStatPacket");
        //onCustomStatPacket(customStatPacket);
    }

    parseTimeSpanCustomStatPacket() {
        let timeSpanCustomStatPacket = new TimeSpanCustomStatPacket(this.buffer);

        console.log("parseTimeSpanCustomStatPacket");
        //onTimeSpanCustomStatPacket(timeSpanCustomStatPacket);
    }

    parseScopeColourPacket() {
        let scopeColourPacket = new ScopeColourPacket(this.buffer);

        console.log("parseScopeColourPacket");
        //onScopeColourPacket(scopeColourPacket);
    }

    parseCustomStatColourPacket() {
        let setCallstackRecordingEnabledPacket = new SetCallstackRecordingEnabledPacket(this.buffer);

        console.log("parseSetCallstackRecordingEnabledPacket");
        //onSetCallstackRecordingEnabledPacket(setCallstackRecordingEnabledPacket);
    }

    parseCustomStatGraphPacket() {
        let customStatGraphPacket = new CustomStatGraphPacket(this.buffer);

        console.log("parseCustomStatGraphPacket");
        //onCustomStatGraphPacket(customStatGraphPacket);
    }

    parseCustomStatUnitPacket() {
        let customStatUnitPacket = new CustomStatUnitPacket(this.buffer);

        console.log("parseCustomStatUnitPacket");
        //onCustomStatUnitPacket(customStatUnitPacket);
    }

    parseBufferEventPacket() {
        let bufferEventPacket = new BufferEventPacket(this.buffer);

        console.log("parseBufferEventPacket");
        //onCustomStatUnitPacket(customStatUnitPacket);
    }

    parse(buffer) {
        this.buffer = new FileBuffer(buffer);
        this.timeSpanPackets = [];
        this.stringMap = new Map();

        console.log(this.parseHeader());

        while (!this.buffer.eof()) {
            let packetTypeAndCPUCore = this.buffer.peekUInt32();
            let packetType = packetTypeAndCPUCore & 0xffff;
            let CPUCore = (packetTypeAndCPUCore >>> 16);

            if (packetType >= PacketType.Connect && packetType <= PacketType.BufferEventPacket) {
                switch (packetType) {
                    case PacketType.Connect:
                        this.parseConnect();
                        break;
                    case PacketType.FrameStart:
                        this.parseFrameStart();
                        break;
                    case PacketType.TimeSpan:
                        this.parseTimeSpan();
                        break;
                    case PacketType.TimeSpanW:
                        this.parseTimeSpanW();
                        break;
                    case PacketType.NamedTimeSpan:
                        this.parseNamedTimeSpan();
                        break;
                    case PacketType.StringLiteralNamedTimeSpan:
                        this.parseStringLiteralNamedTimeSpan();
                        break;
                    case PacketType.ThreadName:
                        this.parseThreadName();
                        break;
                    case PacketType.ThreadOrder:
                        this.parseThreadOrder();
                        break;
                    case PacketType.StringPacket:
                        this.parseStringPacket();
                        break;
                    case PacketType.WStringPacket:
                        this.parseWStringPacket();
                        break;
                    case PacketType.NameAndSourceInfoPacket:
                        this.parseNameAndSourceInfoPacket();
                        break;
                    case PacketType.NameAndSourceInfoPacketW:
                        this.parseNameAndSourceInfoPacketW();
                        break;
                    case PacketType.SourceInfoPacket:
                        this.parseSourceInfoPacket();
                        break;
                    case PacketType.MainThreadPacket:
                        this.parseMainThreadPacket();
                        break;
                    case PacketType.RequestStringLiteralPacket:
                        this.parseRequestStringLiteralPacket();
                        break;
                    case PacketType.SetConditionalScopeMinTimePacket:
                        this.parseSetConditionalScopeMinTimePacket();
                        break;
                    case PacketType.ConnectResponsePacket:
                        this.parseConnectResponsePacket();
                        break;
                    case PacketType.SessionInfoPacket:
                        this.parseSessionInfoPacket();
                        break;
                    case PacketType.RequestRecordedDataPacket:
                        this.parseRequestRecordedDataPacket();
                        break;
                    case PacketType.SessionDetailsPacket:
                        this.parseSessionDetailsPacket();
                        break;
                    case PacketType.ContextSwitchPacket:
                        this.parseContextSwitchPacket();
                        break;
                    case PacketType.ContextSwitchRecordingStartedPacket:
                        this.parseContextSwitchRecordingStartedPacket();
                        break;
                    case PacketType.ProcessNamePacket:
                        this.parseProcessNamePacket();
                        break;
                    case PacketType.CustomStatPacket_Depreciated:
                        this.parseCustomStatPacket_Depreciated();
                        break;
                    case PacketType.StringLiteralTimerNamePacket:
                        this.parseStringLiteralTimerNamePacket();
                        break;
                    case PacketType.HiResTimerScopePacket:
                        this.parseHiResTimerScopePacket();
                        break;
                    case PacketType.LogPacket:
                        this.parseLogPacket();
                        break;
                    case PacketType.EventPacket:
                        this.parseEventPacket();
                        break;
                    case PacketType.StartWaitEventPacket:
                        this.parseStartWaitEventPacket();
                        break;
                    case PacketType.StopWaitEventPacket:
                        this.parseStopWaitEventPacket();
                        break;
                    case PacketType.TriggerWaitEventPacket:
                        this.parseTriggerWaitEventPacket();
                        break;
                    case PacketType.TimeSpanCustomStatPacket_Depreciated:
                        this.parseTimeSpanCustomStatPacket_Depreciated();
                        break;
                    case PacketType.TimeSpanWithCallstack:
                        this.parseTimeSpanWithCallstack();
                        break;
                    case PacketType.TimeSpanWWithCallstack:
                        this.parseTimeSpanWWithCallstack();
                        break;
                    case PacketType.NamedTimeSpanWithCallstack:
                        this.parseNamedTimeSpanWithCallstack();
                        break;
                    case PacketType.StringLiteralNamedTimeSpanWithCallstack:
                        this.parseStringLiteralNamedTimeSpanWithCallstack();
                        break;
                    case PacketType.ModulePacket:
                        this.parseModulePacket();
                        break;
                    case PacketType.SetCallstackRecordingEnabledPacket:
                        this.parseSetCallstackRecordingEnabledPacket();
                        break;
                    case PacketType.CustomStatPacketW:
                        this.parseCustomStatPacketW();
                        break;
                    case PacketType.TimeSpanCustomStatPacketW:
                        this.parseTimeSpanCustomStatPacketW();
                        break;
                    case PacketType.CustomStatPacket:
                        this.parseCustomStatPacket();
                        break;
                    case PacketType.TimeSpanCustomStatPacket:
                        this.parseTimeSpanCustomStatPacket();
                        break;
                    case PacketType.ScopeColourPacket:
                        this.parseScopeColourPacket();
                        break;
                    case PacketType.CustomStatColourPacket:
                        this.parseCustomStatColourPacket();
                        break;
                    case PacketType.CustomStatGraphPacket:
                        this.parseCustomStatGraphPacket();
                        break;
                    case PacketType.CustomStatUnitPacket:
                        this.parseCustomStatUnitPacket();
                        break;
                    case PacketType.BufferEventPacket:
                        this.parseBufferEventPacket();
                        break;
                };
            }
            else {
                assert(false, "Unknown Package Type");
            }
        }
    }

    getString(stringid) {
        return this.stringMap.get(stringid.value);
    }

    getThreadName(threadid) {
        return this.getString(this.threadNamePacketMap.get(threadid).name);
    }

    // sort timeSpan By threadId
    sort() {
        this.timeSpanPackets.sort(
            function (a, b) {
                if (a.threadId < b.threadId) {
                    return -1;
                }
                else if (a.threadId == b.threadId) {
                    return a.startTime.sub(b.startTime);
                }

                return 1;
            });
    }

    link() {
        var callerStack = [];
        this.timeSpanPackets.forEach(
            function (callee, index, array) {
                var tab = "|   ";
                while (callerStack.length != 0 && !TimeSpanPacket.checkCaller(callerStack[callerStack.length - 1], callee)) {
                    callerStack.pop();
                }

                if (callerStack.length != 0)
                    callee.setCaller(callerStack[callerStack.length - 1]);
                else
                    callee.setCaller(null);

                console.log(tab.repeat(callerStack.length) + "+---" + callee.threadId + ","
                    + callee.startTime.value + ", " + callee.getTotalTime() + ", " + callee.getSelfTime() + ", "
                    + this.getString(callee.nameAndSourceInfo));

                callerStack.push(callee);
            }.bind(this));
    }

    print() {
        var callerStack = [];
        this.timeSpanPackets.forEach(
            function (callee, index, array) {
                while (callerStack.length != 0 && !TimeSpanPacket.checkCaller(callerStack[callerStack.length - 1], callee)) {
                    callerStack.pop();
                }

                let nameAndSourceInfos = this.getString(callee.nameAndSourceInfo).split("|");
                let threadid = callee.threadId;

                console.log(
                    {
                        threadName: this.getThreadName(threadid),
                        threadid: threadid,
                        callLevel: callerStack.length, // call level
                        startTime: callee.startTime.value, // starttime
                        totalTime: callee.getTotalTime(), // totalTime
                        name: nameAndSourceInfos[0], // name
                        file: nameAndSourceInfos[1], // file
                        functionNames: nameAndSourceInfos[2], // functionNames
                        line: nameAndSourceInfos[3], // line number
                    }
                );

                callerStack.push(callee);
            }.bind(this));
    }

    getTimeSpan() {
        let timeSpanMap = new Map();

        var callerStack = [];
        this.timeSpanPackets.forEach(
            function (callee, index, array) {
                while (callerStack.length != 0 && !TimeSpanPacket.checkCaller(callerStack[callerStack.length - 1], callee)) {
                    callerStack.pop();
                }

                let nameAndSourceInfos = this.getString(callee.nameAndSourceInfo).split("|");
                let threadid = callee.threadId;

                if (!timeSpanMap.has(threadid))
                    timeSpanMap.set(threadid, []);

                timeSpanMap.get(threadid).push(
                    {
                        threadName: this.getThreadName(threadid),
                        threadid: threadid,
                        callLevel: callerStack.length, // call level
                        startTime: callee.startTime.value, // starttime
                        totalTime: callee.getTotalTime(), // totalTime
                        selfTime: callee.getSelfTime(), // selfTime
                        name: nameAndSourceInfos[0], // name
                        file: nameAndSourceInfos[1], // file
                        functionNames: nameAndSourceInfos[2], // functionNames
                        line: nameAndSourceInfos[3], // line number
                    }
                );

                callerStack.push(callee);
            }.bind(this));

        return timeSpanMap;
    }

    getTimeSpanSorted(sorter) {
        let timeSpanMap = this.getTimeSpan();

        // for each thread, sort function by total Time
        timeSpanMap.forEach(
            function (value, key, map) {
                value.sort(sorter);
            }
        );

        return timeSpanMap;
    }

    getTimeSpanSortedByTotalTime() {
        return this.getTimeSpanSorted(
            function (a, b) {
                if (a.totalTime < b.totalTime)
                    return 1;
                else
                    return -1;
            });
    }

    getTimeSpanSortedBySelfTime() {
        return this.getTimeSpanSorted(
            function (a, b) {
                if (a.selfTime < b.selfTime)
                    return 1;
                else
                    return -1;
            });
    }

    build(buffer) {
        this.parse(buffer);
        this.sort();
        this.link();
    }
}

module.exports = TimeProfileModel;