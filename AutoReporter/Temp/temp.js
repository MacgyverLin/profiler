const assert = require("assert");

function functionData() {
    let b1 = Buffer.from([1, 2, 3, 4, 5, 6, 7, 8]);
    console.log(b1.readUInt8());  // logs 1  
    console.log(b1.readUInt8(0)); // logs 1  
    console.log(b1.readUInt8(1)); // logs 2  
    console.log(b1.readUInt8(7)); // logs 8  
    //console.log(b1.readUInt8(8)); // throws exception  

    let b2 = Buffer.from([1, 0, 2, 0, 3, 0, 4, 0]);
    console.log(b2.readUInt16LE());  // logs 1, read 1,0  
    console.log(b2.readUInt16LE(0)); // logs 1, read 1,0  
    console.log(b2.readUInt16LE(2)); // logs 2, read 2,0  
    console.log(b2.readUInt16LE(6)); // logs 4, read 4,0      

    let b4 = Buffer.from([1, 0, 0, 1, 0, 2]);
    console.log(b4.readUInt16LE(0)); // logs 1, read 1,0  
    console.log(b4.readUInt16BE(2)); // logs 1, read 0,1  
    console.log(b4.readUInt16BE(4)); // logs 2, read 0,2      

    let b5 = Buffer.from([0, 0, 1, 1])
    console.log(b5.slice());  // logs [0, 0, 1, 1]  
    console.log(b5.slice(0)); // logs [0, 0, 1, 1]  
    console.log(b5.slice(0, 2)); // logs [0, 0]  
    console.log(b5.slice(2, 4)); // logs [1, 1];     

    let b6 = Buffer.from([1, 0, 2, 3, 0, 0, 0, 0, 4, 5, 5, 5]);
    console.log(b6.readUInt8(0)); // logs 1  
    console.log(b6.readUInt16BE(1)); // logs 2  
    console.log(b6.readUInt16LE(3)); // logs 3  
    console.log(b6.readUInt32BE(5)); // logs 4  
    console.log(b6.slice(9, 12)); // logs [5, 5, 5]     
}

function* sillyGenerator() {
    yield 1;
    yield 2;
    yield 3;
    yield 4;
};

function request(url) {
    getJSON(url, function (response) {
        generator.next(response);
    });
}

function* getData() {
    var entry1 = yield request('http://some_api/item1');
    var data1 = JSON.parse(entry1);
    var entry2 = yield request('http://some_api/item2');
    var data2 = JSON.parse(entry2);
}

function testGenerator() {
    var generator = sillyGenerator();
    console.log(generator.next());
    console.log(generator.next());
    console.log(generator.next());
    console.log(generator.next());
}


//数据结构对比 增查改删
function testArrayMap() {
    //map和array对比

    let map = new Map();
    let array = [];

    //增
    map.set('t', 1);
    array.push({ t: 1 });
    console.info('map-array', map, array) //{"t"=>1};-[0:{t:1}]

    //查
    let map_exist = map.has('t');
    let array_exist = array.find(item => item.t);//true
    console.info('exist', map_exist, array_exist);//{t:1}

    //改
    map.set('t', 2);
    array.forEach(item => item.t ? 2 : '');
    console.info('map-array-modify', map, array);//{"t"=>2};0:{t: 1}

    //删
    map.delete('t');
    let index = array.findIndex(item => item.t);
    array.splice(index, 1);
    console.info('map-array-empty', map, array);//{};[]
}

function testArraySet() {
    //set和array对比
    let set = new Set();
    let array = [];

    //增
    set.add({ 't': 1 });
    array.push({ t: 1 });
    console.info('set-array', set, array);//0:value{t:1};0:{t: 1}

    //查
    let set_exist = set.has({ t: 1 });
    let array_exist = array.find(item => item.t);
    console.info('exist', set_exist, array_exist);//false;{t:1};

    //改
    set.forEach(item => item.t ? item.t = 2 : '');
    array.forEach(item => item.t ? item.t = 2 : '');
    console.info(set, array);//0:value{t:2};0:{t: 2}

    //删
    set.forEach(item => item.t ? set.delete(item) : '');
    let index = array.findIndex(item => item.t);
    array.splice(index, 1);
    console.info('set-array-empty', set, array);//{};[]
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class FrameProRecordingBuffer {
    constructor(buffer) {
        this.buffer = buffer;
        this.offset = 0;
    }

    readUInt8() {
        let val = this.peekUInt8();
        this.offset += 1;
        return val;
    }

    readUInt16() {
        let val = this.peekUInt16();
        this.offset += 2;
        return val;
    }

    readUInt32() {
        let val = this.peekUInt32();
        this.offset += 4;
        return val;
    }

    readUInt64() {
        let val = this.peekUInt64();
        this.offset += 8;

        return val;
    }

    readInt8() {
        let val = this.peekInt8();
        this.offset += 1;
        return val;
    }

    readInt16() {
        let val = this.peekInt16();
        this.offset += 2;
        return val;
    }

    readInt32() {
        let val = this.peekInt32();
        this.offset += 4;
        return val;
    }

    readInt64() {
        let val = this.peekInt64();
        this.offset += 8;

        return val;
    }

    readFloat() {
        let val = this.peekFloat();
        this.offset += 4;
        return val;
    }

    readDouble() {
        let val = this.peekDouble();
        this.offset += 8;
        return val;
    }

    readEnum() {
        return this.readUInt32();
    }

    readString(size) {
        let val = this.peekString(size);
        this.offset += size;
        return val;
    }

    peekUInt8() {
        let val = this.buffer.readUInt8(this.offset);
        return val;
    }

    peekUInt16() {
        let val = this.buffer.readUInt16LE(this.offset);
        return val;
    }

    peekUInt32() {
        let val = this.buffer.readUInt32LE(this.offset);
        return val;
    }

    peekUInt64() {
        let val = [this.buffer.readUInt32LE(this.offset), this.buffer.readUInt32LE(this.offset + 4)];
        return val;
    }

    peekInt8() {
        let val = this.buffer.readInt8(this.offset);
        return val;
    }

    peekInt16() {
        let val = this.buffer.readInt16LE(this.offset);
        return val;
    }

    peekInt32() {
        let val = this.buffer.readInt32LE(this.offset);
        return val;
    }

    peekInt64() {
        let val = [this.buffer.readInt32LE(this.offset), this.buffer.readInt32LE(this.offset + 4)];
        return val;
    }

    peekFloat() {
        let val = this.buffer.readFloatLE(this.offset);
        return val;
    }

    peekDouble() {
        let val = this.buffer.readDoubleLE(this.offset);
        return val;
    }

    peekEnum() {
        return this.peekUInt32();
    }

    peekString(size) {
        let val = this.buffer.toString('ascii', this.offset, size);
        return val;
    }

    length() {
        return this.buffer.length;
    }

    eof() {
        return this.offset == this.buffer.length;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
var PacketType =
{
    Connect: 0xaabb,
    FrameStart: 0xaabc,
    TimeSpan: 0xaabd,
    TimeSpanW: 0xaabe,
    NamedTimeSpan: 0xaabf,
    StringLiteralNamedTimeSpan: 0xaac0,
    ThreadName: 0xaac1,
    ThreadOrder: 0xaac2,
    StringPacket: 0xaac3,
    WStringPacket: 0xaac4,
    NameAndSourceInfoPacket: 0xaac5,
    NameAndSourceInfoPacketW: 0xaac6,
    SourceInfoPacket: 0xaac7,
    MainThreadPacket: 0xaac8,
    RequestStringLiteralPacket: 0xaac9,
    SetConditionalScopeMinTimePacket: 0xaaca,
    ConnectResponsePacket: 0xaacb,
    SessionInfoPacket: 0xaacc,
    RequestRecordedDataPacket: 0xaacd,
    SessionDetailsPacket: 0xaace,
    ContextSwitchPacket: 0xaacf,
    ContextSwitchRecordingStartedPacket: 0xaad0,
    ProcessNamePacket: 0xaad1,
    CustomStatPacket_Depreciated: 0xaad2,
    StringLiteralTimerNamePacket: 0xaad3,
    HiResTimerScopePacket: 0xaad4,
    LogPacket: 0xaad5,
    EventPacket: 0xaad6,
    StartWaitEventPacket: 0xaad7,
    StopWaitEventPacket: 0xaad8,
    TriggerWaitEventPacket: 0xaad9,
    TimeSpanCustomStatPacket_Depreciated: 0xaada,
    TimeSpanWithCallstack: 0xaadb,
    TimeSpanWWithCallstack: 0xaadc,
    NamedTimeSpanWithCallstack: 0xaadd,
    StringLiteralNamedTimeSpanWithCallstack: 0xaade,
    ModulePacket: 0xaadf,
    SetCallstackRecordingEnabledPacket: 0xaae0,
    CustomStatPacketW: 0xaae1,
    TimeSpanCustomStatPacketW: 0xaae2,
    CustomStatPacket: 0xaae3,
    TimeSpanCustomStatPacket: 0xaae4,
    ScopeColourPacket: 0xaae5,
    CustomStatColourPacket: 0xaae6,
    CustomStatGraphPacket: 0xaae7,
    CustomStatUnitPacket: 0xaae8
};

var Platform = {
    Windows: 0,
    Windows_UWP: 1,
    XBoxOne: 2,
    Unused: 3,
    Linux: 4,
    PS4: 5,
    Android: 6,
    Mac: 7,
    iOS: 8,
    Switch: 9
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function isPow2(value) {
    return (value & (value - 1)) == 0;
}

function alignUpPow2(value, alignment) {
    assert(isPow2(alignment));		// non-pow2 value passed to align function

    mask = alignment - 1;

    return (value + mask) & ~mask;
}

class Long {
    constructor(buffer) {
        this.low = buffer.readUInt32();
        this.high = buffer.readUInt32();
    }
}

class StringId {
    constructor(buffer) {
        this.low = buffer.readUInt32();
        this.high = buffer.readUInt32();
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class ConnectPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.version = buffer.readInt32();
        this.clockFrequency = buffer.readUInt64();
        this.m_ProcessId = buffer.readInt32();
        this.m_Platform = buffer.readEnum();
    }
}

//------------------------------------------------------------------------
class SessionDetailsPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.padding = buffer.readInt32();
        this.name = new StringId(buffer);
        this.buildId = new StringId(buffer);
        this.date = new StringId(buffer);
    }
}

//------------------------------------------------------------------------
class TimeSpanPacket {
    constructor(buffer) {
        this.packetType_AndCore = buffer.readEnum();
        this.threadId = buffer.readInt32();
        this.nameAndSourceInfo = new StringId(buffer);
        this.startTime = new Long(buffer);
        this.endTime = new Long(buffer);
    };
}

//------------------------------------------------------------------------
class TimeSpanCustomStatPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.threadId = buffer.readInt32();
        this.valueType = buffer.readInt32();
        this.padding = buffer.readInt32();
        this.name = new StringId(buffer);
        this.valueInt64 = new Long(buffer);
        this.valueDouble = buffer.readDouble();
        this.time = new Long(buffer);
    }
}

//------------------------------------------------------------------------
class NamedTimeSpanPacket {
    constructor(buffer) {
        this.packetType_AndCore = buffer.readEnum();
        this.threadId = buffer.readInt32();
        this.name = new StringId(buffer);
        this.sourceInfo = new StringId(buffer);
        this.startTime = new Long(buffer);
        this.endTime = new Long(buffer);
    }
}

//------------------------------------------------------------------------
class FrameStartPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.legacy1 = buffer.readInt32();
        this.legacy2 = buffer.readInt32();
        this.padding = buffer.readInt32();
        this.frameStartTime = new Long(buffer);
        this.waitForSendCompleteTime = new Long(buffer);
        this.legacy4 = new Long(buffer);
    }
}

//------------------------------------------------------------------------
class ThreadNamePacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.threadId = buffer.readUInt32();
        this.name = new StringId(buffer);

        console.log(this.packetType);
        console.log(this.threadId);
        console.log(this.name);
    }
}

//------------------------------------------------------------------------
class ThreadOrderPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.padding = buffer.readInt32();
        this.threadName = new StringId(buffer);
    }
}

//------------------------------------------------------------------------
class StringPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.length = buffer.readInt32();
        this.stringId = new StringId(buffer);

        var aligned_string_len = alignUpPow2(this.length, 4);
        this.string = buffer.readString(aligned_string_len);
    }
}

//------------------------------------------------------------------------
class MainThreadPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.threadId = buffer.readInt32();
    }
}

//------------------------------------------------------------------------
class SessionInfoPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.threadId = buffer.readInt32();

        this.sendBufferSize = new Long(buffer);
        this.stringMemorySize = new Long(buffer);
        this.miscMemorySize = new Long(buffer);
        this.recordingFileSize = new Long(buffer);
    }
}

//------------------------------------------------------------------------
class ContextSwitchPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.m_CPUId = buffer.readInt32();
        this.timestamp = new Long(buffer);
        this.processId = buffer.readInt32();
        this.oldThreadId = buffer.readInt32();
        this.newThreadId = buffer.readInt32();
        this.oldThreadState = buffer.readInt32();
        this.oldThreadWaitReason = buffer.readInt32();
        this.padding = buffer.readInt32();
    };
}

const FRAMEPRO_MAX_INLINE_STRING_LENGTH = 256;

//------------------------------------------------------------------------
class ContextSwitchRecordingStartedPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.startedSucessfully = buffer.readInt32();
        this.error = buffer.readString(FRAMEPRO_MAX_INLINE_STRING_LENGTH);
    };
};

//------------------------------------------------------------------------
class ProcessNamePacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.processId = buffer.readInt32();
        this.nameId = new StringId(buffer);
    };
};

//------------------------------------------------------------------------
class CustomStatPacketInt64 {
    constructor(buffer) {
        this.packetTypeAndValueType = buffer.readEnum();
        this.count = buffer.readInt32();
        this.name = new StringId(buffer);
        this.value = new Long(buffer);
    };
};

//------------------------------------------------------------------------
class CustomStatPacketDouble {
    constructor(buffer) {
        this.packetTypeAndValueType = buffer.readEnum();
        this.count = buffer.readInt32();
        this.name = new StringId(buffer);
        this.value = buffer.readDouble();
    };
};

//------------------------------------------------------------------------
class HiResTimer {
    constructor(buffer) {
        this.name = new StringId(buffer);
        this.duration = new Long(buffer);
        this.count = new Long(buffer);
    };
};

class HiResTimerScopePacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.padding = buffer.readInt32();
        this.startTime = new Long(buffer);
        this.endTime = new Long(buffer);

        this.count = buffer.readInt32();
        this.threadId = buffer.readInt32();
    };
};

//------------------------------------------------------------------------
class LogPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.length = buffer.readInt32();
        this.time = new Long(buffer);
    };
};

//------------------------------------------------------------------------
class EventPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.colour = buffer.readUInt32();
        this.name = new StringId(buffer);
        this.time = new Long(buffer);
    };
};

//------------------------------------------------------------------------
class WaitEventPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.thread = buffer.readUInt32();
        this.core = buffer.readUInt32();
        this.padding = buffer.readUInt32();
        this.colour = buffer.readUInt32();
        this.eventId = new Long(buffer);
        this.time = new Long(buffer);
    };
};

//------------------------------------------------------------------------
class CallstackPacket {
    constructor(buffer) {
        // we don't have a packet type here because it always follows a time span packet
        this.callstackId = buffer.readUInt32();
        this.callstackSize = buffer.readUInt32();
    };
};

//------------------------------------------------------------------------
class ScopeColourPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.colour = buffer.readUInt32();
        this.name = new StringId(buffer);
    };
};

//------------------------------------------------------------------------
class CustomStatInfoPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.padding = buffer.readUInt32();
        this.name = new StringId(buffer);
        this.value = new StringId(buffer);
    };
};

//------------------------------------------------------------------------
class CustomStatColourPacket {
    constructor(buffer) {
        this.packetType = buffer.readEnum();
        this.colour = buffer.readUInt32();
        this.name = new StringId(buffer);
    };
};

//------------------------------------------------------------------------
// receive packets
//------------------------------------------------------------------------
//------------------------------------------------------------------------
class RequestStringLiteralPacket {
    constructor(buffer) {
        this.stringId = new StringId(buffer);
        this.stringLiteralType = buffer.readUInt32();
        this.padding = buffer.readUInt32();
        this.name = new StringId(buffer);
    };
};

//------------------------------------------------------------------------
class SetConditionalScopeMinTimePacket {
    constructor(buffer) {
        this.minTime = buffer.readUInt32();
    };
};

//------------------------------------------------------------------------
class ConnectResponsePacket {
    constructor(buffer) {
        this.interactive = buffer.readUInt32();
        this.recordContextSwitches = buffer.readUInt32();
    };
};

//------------------------------------------------------------------------
class RequestRecordedDataPacket {
    constructor(buffer) {
    };
};

//------------------------------------------------------------------------
class SetCallstackRecordingEnabledPacket {
    constructor(buffer) {
        this.enabled = buffer.readInt32();
    };
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class FrameProRecording {
    constructor(buffer) {
        this.buffer = buffer;

        this.timeSpanPackets = [];
        this.namedTimeSpanPackets = [];
    }

    readTimeSpanPacketCore() {
        var timeSpanPacket = new TimeSpanPacket(this.buffer);

        this.timeSpanPackets.push(timeSpanPacket);

        return timeSpanPacket;
    }

    readNamedTimeSpanPacketCore() {
        var namedTimeSpanPacket = new NamedTimeSpanPacketCore(this.buffer);

        this.namedTimeSpanPackets.push(namedTimeSpanPacket);

        return namedTimeSpanPacket;
    }

    readStringCore() {
        var stringPacket = new StringPacket(this.buffer);

        return stringPacket;
    }

    parseHeader() {
        let header = this.buffer.readString(18);
        return header;
    }

    readConnect() {
        this.connectPacket = new ConnectPacket(this.buffer);

        console.log("readConnect");
        // onConnectPacket(this.connectPacket);
    }

    readFrameStart() {
        this.frameStartPacket = new FrameStartPacket(this.buffer);

        console.log("readFrameStart");
        // onFrameStartPacket(frameStartPacket);
    }

    readTimeSpan() {
        var timeSpanPacket = this.readTimeSpanPacketCore();

        console.log("readTimeSpan");
        // onTimeSpan(timeSpanPacket);
    }

    readTimeSpanW() {
        var timeSpanPacketW = this.readTimeSpanPacketCore();

        console.log("readTimeSpanW");
        // onTimeSpanW(timeSpanPacketW);
    }

    readNamedTimeSpan() {
        var namedTimeSpanPacketCore = this.readNamedTimeSpanPacketCore();

        console.log("readNamedTimeSpan");
        // onNamedTimeSpan(namedTimeSpanPacketCore);
    }

    readStringLiteralNamedTimeSpan() {
        var namedTimeSpanPacketCore = this.readNamedTimeSpanPacketCore();

        console.log("readStringLiteralNamedTimeSpan");
        // onStringLiteralNamedTimeSpan(namedTimeSpanPacketCore);
    }

    readThreadName() {
        this.threadNamePacket = new ThreadNamePacket(this.buffer);

        console.log("readThreadName");
        // onThreadName(threadNamePacket);
    }

    readThreadOrder() {
        this.threadOrderPacket = new ThreadOrderPacket(this.buffer);

        console.log("readThreadOrder");
        // onThreadOrder(threadOrderPacket);
    }

    readStringPacket() {
        var stringPacket = this.readStringCore();

        console.log("readStringPacket");
        // onStringPacket(this.stringPacket);
    }

    readWStringPacket() {
        var stringPacket = this.readStringCore();

        console.log("readWStringPacket");
        // onWStringPacket(this.stringPacket);
    }

    readNameAndSourceInfoPacket() {
        var stringPacket = this.readStringCore();

        console.log("readNameAndSourceInfoPacket");
        // onNameAndSourceInfoPacket(this.stringPacket);
    }

    readNameAndSourceInfoPacketW() {
        var stringPacket = this.readStringCore();

        console.log("readNameAndSourceInfoPacketW");
        // onNameAndSourceInfoPacketW(this.stringPacket);
    }

    readSourceInfoPacket() {
        var stringPacket = this.readStringCore();

        console.log("readSourceInfoPacket");
        // onSourceInfoPacket(this.stringPacket);
    }

    readMainThreadPacket() {
        this.mainThreadPacket = new MainThreadPacket(this.buffer);

        console.log("readMainThreadPacket");
        // onMainThreadPacket(this.mainThreadPacket);
    }
    readRequestStringLiteralPacket() {
        this.requestStringLiteralPacket = new RequestStringLiteralPacket(this.buffer);

        console.log("readRequestStringLiteralPacket");
        // onRequestStringLiteralPacket(this.requestStringLiteralPacket);
    }

    readSetConditionalScopeMinTimePacket() {
        this.setConditionalScopeMinTimePacket = new SetConditionalScopeMinTimePacket(this.buffer);

        console.log("readSetConditionalScopeMinTimePacket");
        // onSetConditionalScopeMinTimePacket(this.setConditionalScopeMinTimePacket);
    }
    readConnectResponsePacket() {
        this.connectResponsePacket = new ConnectResponsePacket(this.buffer);

        console.log("readConnectResponsePacket");
        // onConnectResponsePacket(this.readConnectResponsePacket);
    }
    readSessionInfoPacket() {
        this.sessionInfoPacket = new SessionInfoPacket(this.buffer);

        console.log("readSessionInfoPacket");
        // onSessionInfoPacket(this.readSessionInfoPacket);
    }
    readRequestRecordedDataPacket() {
        this.requestRecordedDataPacket = new RequestRecordedDataPacket(this.buffer);

        console.log("readRequestRecordedDataPacket");
        // onRequestRecordedDataPacket(this.requestRecordedDataPacket);
    }
    readSessionDetailsPacket() {
        this.sessionDetailsPacket = new SessionDetailsPacket(this.buffer);

        console.log("readSessionDetailsPacket");
        // onSessionDetailsPacket(this.sessionDetailsPacket);
    }
    readContextSwitchPacket() {
        this.contextSwitchPacket = new ContextSwitchPacket(this.buffer);

        console.log("readContextSwitchPacket");
        // onContextSwitchPacket(this.contextSwitchPacket);
    }
    readContextSwitchRecordingStartedPacket() {
        this.contextSwitchRecordingStartedPacket = new ContextSwitchRecordingStartedPacket(this.buffer);

        console.log("readContextSwitchRecordingStartedPacket");
        // onContextSwitchRecordingStartedPacket(this.contextSwitchRecordingStartedPacket);
    }

    readProcessNamePacket() {
        this.processNamePacket = new ProcessNamePacket(this.buffer);

        console.log("readProcessNamePacket");
        // onProcessNamePacket(this.processNamePacket);
    }

    readCustomStatPacket_Depreciated() {
        console.log("readCustomStatPacket_Depreciated");
        // onCustomStatPacket_Depreciated();
    }

    readStringLiteralTimerNamePacket() {
        var stringPacket = this.readStringCore();

        console.log("readStringLiteralTimerNamePacket");
        // onStringLiteralTimerNamePacket(stringPacket);
    }

    readHiResTimerScopePacket() {
        this.hiResTimerScopePacket = new HiResTimerScopePacket(this.buffer);

        console.log("readHiResTimerScopePacket");
        // onHiResTimerScopePacket(this.hiResTimerScopePacket);
    }

    readLogPacket() {
        this.logPacket = new LogPacket(this.buffer);

        console.log("readLogPacket");
        // onLogPacket(this.logPacket);
    }

    readEventPacket() {
        this.eventPacket = new EventPacket(this.buffer);

        console.log("readEventPacket");
        // onEventPacket(this.eventPacket);
    }

    readStartWaitEventPacket() {
        this.startWaitEventPacket = new WaitEventPacket(this.buffer);

        console.log("readStartWaitEventPacket");
        // onStartWaitEventPacket(this.startWaitEventPacket);
    }
    readStopWaitEventPacket() {
        this.stopWaitEventPacket = new WaitEventPacket(this.buffer);

        console.log("readStopWaitEventPacket");
        // onStopWaitEventPacket(this.stopWaitEventPacket);
    }

    readTriggerWaitEventPacket() {
        this.triggerWaitEventPacket = new WaitEventPacket(this.buffer);

        console.log("readStopWaitEventPacket");
        // onStopWaitEventPacket(this.stopWaitEventPacket);
    }

    readTimeSpanCustomStatPacket_Depreciated() {
        console.log("readTimeSpanCustomStatPacket_Depreciated");
        // onTimeSpanCustomStatPacket_Depreciated();
    }

    readTimeSpanWithCallstack() {
        this.timeSpanPacket = new TimeSpanPacket(this.buffer);
        this.callstackPacket = new CallstackPacket(this.buffer);

        if (this.callstackPacket.callstackSize)
            buffer.readString(this.callstackPacket.callstackSize);

        console.log("readTimeSpanWithCallstack");
        //onTimeSpanWithCallstack(timeSpanPacket, callstackPacket);
    }

    readTimeSpanWWithCallstack() {
        this.timeSpanPacket = new TimeSpanPacket(this.buffer);
        this.callstackPacket = new CallstackPacket(this.buffer);

        if (this.callstackPacket.callstackSize)
            buffer.readString(this.callstackPacket.callstackSize);

        console.log("readTimeSpanWWithCallstack");
        //onTimeSpanWWithCallstack(timeSpanPacket, callstackPacket);
    }

    readNamedTimeSpanWithCallstack() {
        let namedTimeSpanPacket = new NamedTimeSpanPacket(this.buffer);
        let callstackPacket = new CallstackPacket(this.buffer);

        if (this.callstackPacket.callstackSize)
            buffer.readString(this.callstackPacket.callstackSize);

        console.log("readNamedTimeSpanWithCallstack");
        //onNamedTimeSpanWithCallstack(namedTimeSpanPacket, callstackPacket);
    }

    readStringLiteralNamedTimeSpanWithCallstack() {
        let namedTimeSpanPacket = new NamedTimeSpanPacket(this.buffer);
        let callstackPacket = new CallstackPacket(this.buffer);

        if (this.callstackPacket.callstackSize)
            buffer.readString(this.callstackPacket.callstackSize);

        console.log("readStringLiteralNamedTimeSpanWithCallstack");
        //onStringLiteralNamedTimeSpanWithCallstack(namedTimeSpanPacket, callstackPacket);
    }

    readModulePacket() {
        let modulePacket = new ModulePacket(this.buffer);

        console.log("readModulePacket");
        //onModulePacket(modulePacket);
    }

    readSetCallstackRecordingEnabledPacket() {
        let setCallstackRecordingEnabledPacket = new SetCallstackRecordingEnabledPacket(this.buffer);

        console.log("readSetCallstackRecordingEnabledPacket");
        //onSetCallstackRecordingEnabledPacket(setCallstackRecordingEnabledPacket);
    }

    readCustomStatPacketW() {
        let customStatPacketW = new CustomStatPacketW(this.buffer);

        console.log("readCustomStatPacketW");
        //onCustomStatPacketW(customStatPacketW);
    }

    readTimeSpanCustomStatPacketW() {
        let timeSpanCustomStatPacketW = new TimeSpanCustomStatPacketW(this.buffer);

        console.log("readTimeSpanCustomStatPacketW");
        //onTimeSpanCustomStatPacketW(timeSpanCustomStatPacketW);
    }

    readCustomStatPacket() {
        let customStatPacket = new CustomStatPacket(this.buffer);

        console.log("readCustomStatPacket");
        //onCustomStatPacket(customStatPacket);
    }

    readTimeSpanCustomStatPacket() {
        let timeSpanCustomStatPacket = new TimeSpanCustomStatPacket(this.buffer);

        console.log("readTimeSpanCustomStatPacket");
        //onTimeSpanCustomStatPacket(timeSpanCustomStatPacket);
    }

    readScopeColourPacket() {
        let scopeColourPacket = new ScopeColourPacket(this.buffer);

        console.log("readScopeColourPacket");
        //onScopeColourPacket(scopeColourPacket);
    }

    readCustomStatColourPacket() {
        let setCallstackRecordingEnabledPacket = new SetCallstackRecordingEnabledPacket(this.buffer);

        console.log("readSetCallstackRecordingEnabledPacket");
        //onSetCallstackRecordingEnabledPacket(setCallstackRecordingEnabledPacket);
    }

    readCustomStatGraphPacket() {
        let customStatGraphPacket = new CustomStatGraphPacket(this.buffer);

        console.log("readCustomStatGraphPacket");
        //onCustomStatGraphPacket(customStatGraphPacket);
    }

    readCustomStatUnitPacket() {
        let customStatUnitPacket = new CustomStatUnitPacket(this.buffer);

        console.log("readCustomStatUnitPacket");
        //onCustomStatUnitPacket(customStatUnitPacket);
    }

    update() {
        console.log(this.parseHeader());

        while (!this.buffer.eof()) {
            let packetTypeAndCPUCore = this.buffer.peekUInt32();
            let packetType = packetTypeAndCPUCore & 0xffff;
            let CPUCore = (packetTypeAndCPUCore >>> 16);
            console.log(packetTypeAndCPUCore);
            console.log(packetType);
            console.log(CPUCore);

            if (packetType >= PacketType.Connect && packetType <= PacketType.CustomStatUnitPacket) {
                switch (packetType) {
                    case PacketType.Connect:
                        this.readConnect();
                        break;
                    case PacketType.FrameStart:
                        this.readFrameStart();
                        break;
                    case PacketType.TimeSpan:
                        this.readTimeSpan();
                        break;
                    case PacketType.TimeSpanW:
                        this.readTimeSpanW();
                        break;
                    case PacketType.NamedTimeSpan:
                        this.readNamedTimeSpan();
                        break;
                    case PacketType.StringLiteralNamedTimeSpan:
                        this.readStringLiteralNamedTimeSpan();
                        break;
                    case PacketType.ThreadName:
                        this.readThreadName();
                        break;
                    case PacketType.ThreadOrder:
                        this.readThreadOrder();
                        break;
                    case PacketType.StringPacket:
                        this.readStringPacket();
                        break;
                    case PacketType.WStringPacket:
                        this.readWStringPacket();
                        break;
                    case PacketType.NameAndSourceInfoPacket:
                        this.readNameAndSourceInfoPacket();
                        break;
                    case PacketType.NameAndSourceInfoPacketW:
                        this.readNameAndSourceInfoPacketW();
                        break;
                    case PacketType.SourceInfoPacket:
                        this.readSourceInfoPacket();
                        break;
                    case PacketType.MainThreadPacket:
                        this.readMainThreadPacket();
                        break;
                    case PacketType.RequestStringLiteralPacket:
                        this.readRequestStringLiteralPacket();
                        break;
                    case PacketType.SetConditionalScopeMinTimePacket:
                        this.readSetConditionalScopeMinTimePacket();
                        break;
                    case PacketType.ConnectResponsePacket:
                        this.readConnectResponsePacket();
                        break;
                    case PacketType.SessionInfoPacket:
                        this.readSessionInfoPacket();
                        break;
                    case PacketType.RequestRecordedDataPacket:
                        this.readRequestRecordedDataPacket();
                        break;
                    case PacketType.SessionDetailsPacket:
                        this.readSessionDetailsPacket();
                        break;
                    case PacketType.ContextSwitchPacket:
                        this.readContextSwitchPacket();
                        break;
                    case PacketType.ContextSwitchRecordingStartedPacket:
                        this.readContextSwitchRecordingStartedPacket();
                        break;
                    case PacketType.ProcessNamePacket:
                        this.readProcessNamePacket();
                        break;
                    case PacketType.CustomStatPacket_Depreciated:
                        this.readCustomStatPacket_Depreciated();
                        break;
                    case PacketType.StringLiteralTimerNamePacket:
                        this.readStringLiteralTimerNamePacket();
                        break;
                    case PacketType.HiResTimerScopePacket:
                        this.readHiResTimerScopePacket();
                        break;
                    case PacketType.LogPacket:
                        this.readLogPacket();
                        break;
                    case PacketType.EventPacket:
                        this.readEventPacket();
                        break;
                    case PacketType.StartWaitEventPacket:
                        this.readStartWaitEventPacket();
                        break;
                    case PacketType.StopWaitEventPacket:
                        this.readStopWaitEventPacket();
                        break;
                    case PacketType.TriggerWaitEventPacket:
                        this.readTriggerWaitEventPacket();
                        break;
                    case PacketType.TimeSpanCustomStatPacket_Depreciated:
                        this.readTimeSpanCustomStatPacket_Depreciated();
                        break;
                    case PacketType.TimeSpanWithCallstack:
                        this.readTimeSpanWithCallstack();
                        break;
                    case PacketType.TimeSpanWWithCallstack:
                        this.readTimeSpanWWithCallstack();
                        break;
                    case PacketType.NamedTimeSpanWithCallstack:
                        this.readNamedTimeSpanWithCallstack();
                        break;
                    case PacketType.StringLiteralNamedTimeSpanWithCallstack:
                        this.readStringLiteralNamedTimeSpanWithCallstack();
                        break;
                    case PacketType.ModulePacket:
                        this.readModulePacket();
                        break;
                    case PacketType.SetCallstackRecordingEnabledPacket:
                        this.readSetCallstackRecordingEnabledPacket();
                        break;
                    case PacketType.CustomStatPacketW:
                        this.readCustomStatPacketW();
                        break;
                    case PacketType.TimeSpanCustomStatPacketW:
                        this.readTimeSpanCustomStatPacketW();
                        break;
                    case PacketType.CustomStatPacket:
                        this.readCustomStatPacket();
                        break;
                    case PacketType.TimeSpanCustomStatPacket:
                        this.readTimeSpanCustomStatPacket();
                        break;
                    case PacketType.ScopeColourPacket:
                        this.readScopeColourPacket();
                        break;
                    case PacketType.CustomStatColourPacket:
                        this.readCustomStatColourPacket();
                        break;
                    case PacketType.CustomStatGraphPacket:
                        this.readCustomStatGraphPacket();
                        break;
                    case PacketType.CustomStatUnitPacket:
                        this.readCustomStatUnitPacket();
                        break;
                };
            }
            else {
                assert(false, "Unknown Package Type");
            }
        }
    }
}

fs.readFile('mac.framepro_recording', function (err, data) {
    let frameProRecording = new FrameProRecording(new FrameProRecordingBuffer(data));
    frameProRecording.update();
});