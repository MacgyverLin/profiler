const assert = require("assert");
const View = require("./MVC/View");
const TimeProfileController = require("./TimeProfileController");

class TimeProfileView extends View {
    constructor() {
        super();
    }

    update(timeProfileController) {
    }
}

module.exports = TimeProfileView;