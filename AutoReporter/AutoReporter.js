/*
var http = require('http');
var fs = require('fs');
var dt = require('./myfirstmodule');

http.createServer(function (req, res) {
    fs.readFile('demofile1.html', function (err, data) {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(data);
        res.write(dt.myDateTime())
        res.end();
    });
}).listen(8080);
*/

const http = require('http');
const fs = require('fs');
const TimeProfileModel = require('./TimeProfileModel');
const TimeProfileView = require('./TimeProfileView');
const TimeProfileController = require('./TimeProfileController');
const TestModel = require('./MVC/TestModel');
const TestView = require('./MVC/TestView');
const TestController = require('./MVC/TestController');
const config = require('./Config');

// const filename = "LoaderFiber_android_05161740(1).framepro_recording";
const filename = "mac.framepro_recording";

fs.readFile(filename, function (err, data) {
    //let testController = new TestController(new TestModel(), new TestView());
    //testController.setLength1Inch(10);
    //testController.setLength2Inch(20);

    console.log(config.timeProfiler.maxSelfTimeDisplay);
    console.log(config.timeProfiler.maxTotalTimeDisplay);

    // load data
    var timeProfileModel = new TimeProfileModel();
    var timeProfileView = new TimeProfileView();
    var timeProfileController = new TimeProfileController(timeProfileModel, timeProfileView);
    timeProfileController.build(data);

    // get TimeSpan Data Sorted by thread Id
    var timeSpanMap = timeProfileModel.getTimeSpan();
    timeSpanMap.forEach(
        function (value, key, map) {
            value.forEach(
                function (v, index, array) {
                    console.log(v.threadid + " " + v.threadName + " " + v.name + " " + v.totalTime + " " + v.selfTime);
                }
            );
        }
    );

    // get TimeSpan Data Sorted by thread Id then sorted by Total Time
    timeSpanMap = timeProfileModel.getTimeSpanSortedByTotalTime();
    timeSpanMap.forEach(
        function (value, key, map) {
            value.forEach(
                function (v, index, array) {
                    console.log(v.threadid + " " + v.threadName + " " + v.name + " " + v.totalTime + " " + v.selfTime);
                }
            );
        }
    );

    // get TimeSpan Data Sorted by thread Id then sorted by Self Time
    timeSpanMap = timeProfileModel.getTimeSpanSortedBySelfTime();
    timeSpanMap.forEach(
        function (value, key, map) {
            value.forEach(
                function (v, index, array) {
                    console.log(v.threadid + " " + v.threadName + " " + v.name + " " + v.totalTime + " " + v.selfTime);
                }
            );
        }
    );
});