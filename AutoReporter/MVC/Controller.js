var Model = require('./Model');
var Vodel = require('./View');

class Controller {
    constructor(model, view) {
        this.model = model;
        this.view = view;
    }

    getModel() {
        return this.controller;
    }

    getView() {
        return this.view;
    }
}

module.exports = Controller;