var View = require('./View');
var TestController = require('./TestController');

class TestView extends View {
    constructor() {
        super();
    }

    update(controller) {
        console.log(controller.getAreaCM());
    }
}

module.exports = TestView;