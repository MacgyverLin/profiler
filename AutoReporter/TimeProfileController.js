const assert = require("assert");
const Controller = require("./MVC/Controller");
const TimeProfileModel = require("./TimeProfileModel");
const TimeProfileView = require("./TimeProfileView");

class TimeProfileController extends Controller {
    constructor(timeProfileModel, timeProfileView) {
        super(timeProfileModel, timeProfileView);
    }

    build(data) {
        this.model.build(data);
        this.view.update(this);
    }
}

module.exports = TimeProfileController;