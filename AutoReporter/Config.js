var config =
{
    timeProfiler: {
        maxSelfTimeDisplay: 3,
        maxTotalTimeDisplay: 6
    }
};

module.exports = config;